<?php $__env->startSection('title', trans('labels.dashboard.acl.users.header') . ' | ' . trans('labels.dashboard.acl.users.create')); ?>

<?php $__env->startSection('page-header'); ?>
    <h1>
        <?php echo e(trans('labels.dashboard.acl.users.header')); ?>

        <small><?php echo e(trans('labels.dashboard.acl.users.create')); ?></small>
    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo Form::open(['route' => 'dashboard.users.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']); ?>


        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo e(trans('labels.dashboard.acl.users.create')); ?></h3>
        </div>

            <div class="box-body">
                <div class="form-group">
                    <?php echo Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']); ?>

                    <div class="col-lg-10">
                        <?php echo Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']); ?>

                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <?php echo Form::label('email', 'Email', ['class' => 'col-lg-2 control-label']); ?>

                    <div class="col-lg-10">
                        <?php echo Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']); ?>

                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <?php echo Form::label('password', 'Password', ['class' => 'col-lg-2 control-label', 'placeholder' => 'Password']); ?>

                    <div class="col-lg-10">
                        <?php echo Form::password('password', ['class' => 'form-control']); ?>

                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <?php echo Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-lg-2 control-label', 'placeholder' => 'Confirm Password']); ?>

                    <div class="col-lg-10">
                        <?php echo Form::password('password_confirmation', ['class' => 'form-control']); ?>

                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">Confirmed</label>
                    <div class="col-lg-1">
                        <input type="checkbox" value="1" name="confirmed" checked="checked" />
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">Send Confirmation Email<br/>
                        <small>(If confirmed is off)</small>
                    </label>
                    <div class="col-lg-1">
                        <input type="checkbox" value="1" name="confirmation_email" />
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">Associated Roles</label>
                    <div class="col-lg-4">
                        <?php if(count($roles) > 0): ?>
                            <?php foreach($roles as $role): ?>
                                <input type="checkbox" value="<?php echo e($role->id); ?>" name="assignees_roles[]" id="role-<?php echo e($role->id); ?>" />
                                <label for="role-<?php echo e($role->id); ?>"><?php echo $role->name; ?></label>
                                <a href="#" data-role="role_<?php echo e($role->id); ?>" class="show-permissions small">
                                    (
                                        <span class="show-text">Show</span>
                                        <span class="hide-text hidden">Hide</span>
                                        Permissions
                                    )
                                </a>
                                <br/>
                                <div class="permission-list hidden" data-role="role_<?php echo e($role->id); ?>">
                                    <?php if($role->all): ?>
                                        All Permissions<br/><br/>
                                    <?php else: ?>
                                        <?php if(count($role->permissions) > 0): ?>
                                            <blockquote class="small"><?php /*
                                        */ ?><?php foreach($role->permissions as $perm): ?><?php /*
                                            */ ?><?php echo e($perm->display_name); ?><br/>
                                                <?php endforeach; ?>
                                            </blockquote>
                                        <?php else: ?>
                                            No Permissions<br/><br/>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div><!--permission list-->
                            <?php endforeach; ?>
                        <?php else: ?>
                            No Roles
                        <?php endif; ?>
                    </div>
                </div><!--form control-->
                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="Create" />
                    <a href="<?php echo e(route('dashboard.users.index')); ?>" class="btn btn-danger">Cancel</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('after-scripts-end'); ?>
    <?php echo Html::script('assets/js/acl/permission/script.js'); ?>

    <?php echo Html::script('assets/js/acl/user/script.js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>