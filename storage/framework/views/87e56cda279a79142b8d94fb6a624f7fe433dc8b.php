<?php $__env->startSection('title', trans('labels.dashboard.acl.roles.header')); ?>

<?php $__env->startSection('page-header'); ?>
    <h1><?php echo e(trans('labels.dashboard.acl.roles.header')); ?></h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo e(trans('labels.dashboard.acl.roles.header')); ?></h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Role</th>
                        <th>Permissions</th>
                        <th>Number of Users</th>
                        <th>Sort</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($roles as $role): ?>
                            <tr>
                                <td><?php echo $role->name; ?></td>
                                <td>
                                    <?php if($role->all): ?>
                                        <span class="label label-success">All</span>
                                    <?php else: ?>
                                        <?php if(count($role->permissions) > 0): ?>
                                            <div style="font-size:.7em">
                                                <?php foreach($role->permissions as $permission): ?>
                                                    <?php echo $permission->name; ?><br/>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php else: ?>
                                            <span class="label label-danger">None</span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $role->users()->count(); ?></td>
                                <td><?php echo $role->sort; ?></td>
                                <td><?php echo $role->action_buttons; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                <?php echo e($roles->total()); ?> <?php echo e(trans_choice('labels.dashboard.acl.roles.table.total', $roles->total())); ?>

            </div>

            <div class="pull-right">
                <?php echo e($roles->render()); ?>

            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>