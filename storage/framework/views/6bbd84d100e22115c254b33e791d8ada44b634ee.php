<?php $__env->startSection('content'); ?>
    <div class="login-box">
        <div class="login-logo">
            <a href="/"><?php echo e(config('app.app_name')); ?></a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <form role="form" method="POST" action="<?php echo e(url('/login')); ?>">
                <?php echo csrf_field(); ?>


                <?php /*Form Input for Email*/ ?>
                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?> has-feedback">
                    <input type="email" class="form-control" placeholder="Email" name="email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                    <?php if($errors->has('email')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <?php /*Form Input for Password*/ ?>
                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?> has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                    <?php if($errors->has('password')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group">
                    <div class="col-xs-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            <i class="fa fa-btn fa-sign-in"></i>Login
                        </button>
                    </div>
                    <a class="btn btn-link" href="<?php echo e(url('/password/reset')); ?>">Forgot Your Password?</a>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.login-template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>