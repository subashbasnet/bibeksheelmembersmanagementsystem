<?php $__env->startSection('content'); ?>
        <!-- Content Wrapper. Contains page content -->
<div class="content-wrappera">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 align="center">
            <a href="/"> <?php echo e(config('app.app_name')); ?> </a>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p>
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href="../../index.html">return to dashboard</a> or try using the search form.
                </p>
                <form class="search-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-warning btn-flat"><i
                                        class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.landingpage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>