<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version 1.0
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"><?php echo app_name(); ?></a>.</strong> <?php echo e(trans('strings.dashboard.general.all_rights_reserved')); ?>

</footer>