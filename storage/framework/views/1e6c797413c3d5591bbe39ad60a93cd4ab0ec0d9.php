<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(asset('/assets/img/avatar.png')); ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo access()->user()->name; ?></p>
                <!-- Status -->
                <a href="#"><i
                            class="fa fa-circle text-success"></i> <?php echo e(trans('strings.dashboard.general.status.online')); ?>

                </a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <?php /*<form action="#" method="get" class="sidebar-form">*/ ?>
            <?php /*<div class="input-group">*/ ?>
                <?php /*<input type="text" name="q" class="form-control"*/ ?>
                       <?php /*placeholder="<?php echo e(trans('strings.dashboard.general.search_placeholder')); ?>"/>*/ ?>
                  <?php /*<span class="input-group-btn">*/ ?>
                    <?php /*<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i*/ ?>
                                <?php /*class="fa fa-search"></i></button>*/ ?>
                  <?php /*</span>*/ ?>
            <?php /*</div>*/ ?>
        <?php /*</form>*/ ?>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"><?php echo e(trans('menus.dashboard.sidebar.general')); ?></li>

            <!-- Optionally, you can add icons to the links -->
            <li class="<?php echo e(Active::pattern('/')); ?>">
                <a href="<?php echo route('dashboard'); ?>"><span><?php echo e(trans('menus.dashboard.sidebar.dashboard')); ?></span></a>
            </li>

            <?php if (access()->allow('view-access-management')): ?>
                <li class="<?php echo e(Active::pattern('dashboard*/users')); ?> treeview">
                    <a href="#">
                        <span><?php echo e(trans('menus.dashboard.acl.title')); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu <?php echo e(Active::pattern('dashboard/users*', 'menu-open')); ?>" style="display: none; <?php echo e(Active::pattern('dashboard/users*', 'display: block;')); ?>">
                        <?php /*Users Sidebar Menu*/ ?>
                        <li class="<?php echo e(Active::pattern('dashboard/users*')); ?> treeview">
                            <a href="#">
                                <span><?php echo e(trans('menus.dashboard.acl.users.main')); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu <?php echo e(Active::pattern('dashboard/users*', 'menu-open')); ?>" style="display: none; <?php echo e(Active::pattern('dashboard/users*', 'display: block;')); ?>">
                                <li class="<?php echo e(Active::pattern('dashboard/users/create')); ?>">
                                    <a href="<?php echo url('dashboard/users/create'); ?>">
                                        <i class="fa fa-user-plus"></i>
                                        <?php echo e(trans('menus.dashboard.acl.users.create')); ?>

                                    </a>
                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/users')); ?>">
                                    <a href="<?php echo url('dashboard/users'); ?>">
                                        <i class="fa fa-users"></i>
                                        <?php echo e(trans('menus.dashboard.acl.users.all')); ?>

                                    </a>
                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/users/deactivated')); ?>">
                                    <a href="<?php echo url('dashboard/users/deactivated'); ?>">
                                        <i class="fa fa-user-secret"></i>
                                        <?php echo e(trans('menus.dashboard.acl.users.deactivated')); ?>

                                    </a>

                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/users/deleted')); ?>">
                                    <a href="<?php echo url('dashboard/users/deleted'); ?>">
                                        <i class="fa fa-user-times"></i>
                                        <?php echo e(trans('menus.dashboard.acl.users.deleted')); ?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php /* Roles Sidebar Menu */ ?>
                        <li class="<?php echo e(Active::pattern('dashboard/roles*')); ?> treeview">
                            <a href="#">
                                <span><?php echo e(trans('menus.dashboard.acl.roles.main')); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu <?php echo e(Active::pattern('dashboard/roles*', 'menu-open')); ?>" style="display: none; <?php echo e(Active::pattern('dashboard/roles*', 'display: block;')); ?>">
                                <li class="<?php echo e(Active::pattern('dashboard/roles/create')); ?>">
                                    <a href="<?php echo url('dashboard/roles/create'); ?>">
                                        <i class="fa fa-circle-o"></i>
                                        <?php echo e(trans('menus.dashboard.acl.roles.create')); ?>

                                    </a>
                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/roles')); ?>">
                                    <a href="<?php echo url('dashboard/roles'); ?>">
                                        <i class="fa fa-circle-o"></i>
                                        <?php echo e(trans('menus.dashboard.acl.roles.all')); ?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php /* Permissions Sidebar Menu */ ?>
                        <li class="<?php echo e(Active::pattern('dashboard/permissions*')); ?> treeview">
                            <a href="#">
                                <span><?php echo e(trans('menus.dashboard.acl.permissions.main')); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu <?php echo e(Active::pattern('dashboard/permission*', 'menu-open')); ?>" style="display: none; <?php echo e(Active::pattern('dashboard/permission*', 'display: block;')); ?>">
                                <li class="<?php echo e(Active::pattern('dashboard/permissions/create')); ?>">
                                    <a href="<?php echo url('dashboard/permissions/create'); ?>">
                                        <i class="fa fa-user-plus"></i>
                                        <?php echo e(trans('menus.dashboard.acl.permissions.create')); ?>

                                    </a>
                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/permissions#all-permissions')); ?>">
                                    <a href="<?php echo url('dashboard/permissions#all-permissions'); ?>">
                                        <i class="fa fa-users"></i>
                                        <?php echo e(trans('menus.dashboard.acl.permissions.all')); ?>

                                    </a>
                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/permission-group/create')); ?>">
                                    <a href="<?php echo url('dashboard/permission-group/create'); ?>">
                                        <i class="fa fa-user-secret"></i>
                                        <?php echo e(trans('menus.dashboard.acl.permissions.groups.create')); ?>

                                    </a>

                                </li>
                                <li class="<?php echo e(Active::pattern('dashboard/permissions/groups')); ?>">
                                    <a href="<?php echo url('dashboard/permissions'); ?>">
                                        <i class="fa fa-user-times"></i>
                                        <?php echo e(trans('menus.dashboard.acl.permissions.groups.main')); ?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (access()->allow('manage-members')): ?>
                <li class="<?php echo e(Active::pattern('dashboard/members/*')); ?> treeview">
                    <a href="#">
                        <span><?php echo e(trans('menus.dashboard.member.main')); ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu <?php echo e(Active::pattern('dashboard/members*', 'menu-open')); ?>" style="display: none; <?php echo e(Active::pattern('dashboard/members*', 'display: block;')); ?>">
                        <?php if (access()->allow('create-members')): ?>
                            <li class="<?php echo e(Active::pattern('dashboard/members/create')); ?>">
                                <a href="<?php echo route('dashboard.members.create'); ?>">
                                    <i class="fa fa-user-plus"></i>
                                    <?php echo e(trans('menus.dashboard.member.add')); ?>

                                </a>
                            </li>
                        <?php endif; ?>
                        <li class="<?php echo e(Active::pattern('dashboard/members')); ?>">
                            <a href="<?php echo route('dashboard.members.index'); ?>">
                                <i class="fa fa-user"></i>
                                <?php echo e(trans('menus.dashboard.member.view')); ?>

                            </a>
                        </li>
                        <li class="<?php echo e(Active::pattern('dashboard/members/deactivated')); ?>">
                            <a href="<?php echo url('dashboard/members/inactive'); ?>">
                                <i class="fa fa-user-md"></i>
                                <?php echo e(trans('menus.dashboard.member.inactive')); ?>

                            </a>
                        </li>
                        <?php if (access()->allow('restore-members')): ?>
                            <li class="<?php echo e(Active::pattern('dashboard/members/deleted')); ?>">
                                <a href="<?php echo url('dashboard/members/deleted'); ?>">
                                    <i class="fa fa-user-times"></i>
                                    <?php echo e(trans('menus.dashboard.member.deleted')); ?>

                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </section>
</aside>