<?php $__env->startSection('title', 'Role Management' . ' | ' . 'Edit Role'); ?>

<?php $__env->startSection('page-header'); ?>
    <h1>
        Roles Management
        <small>Edit Role</small>
    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after-styles-end'); ?>
    <?php echo Html::style('assets\css\jstree\themes\default\style.min.css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo Form::model($role, ['route' => ['dashboard.roles.update', $role->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role']); ?>


    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Role</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                <?php echo Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']); ?>

                <div class="col-lg-10">
                    <?php echo Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']); ?>

                </div>
            </div><!--form control-->

            <div class="form-group">
                <?php echo Form::label('slug', 'Slug', ['class' => 'col-lg-2 control-label']); ?>

                <div class="col-lg-10">
                    <?php echo Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'slug']); ?>

                </div>
            </div>

            <div class="form-group">
                <?php echo Form::label('name', 'Sort', ['class' => 'col-lg-2 control-label']); ?>

                <div class="col-lg-10">
                    <?php echo Form::input('number','sort', null, ['class' => 'form-control', 'placeholder' => 'Sort']); ?>

                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Associated Permissions</label>
                <div class="col-lg-10">
                    <div id="available-permissions">
                        <div class="row">

                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>
                                    <?php echo trans('strings.dashboard.acl.permissions.associated-permissions-explanation'); ?>

                                </div>
                            </div>

                            <div class="col-lg-6">
                                <p><strong>Grouped Permissions</strong></p>

                                <?php if($groups->count()): ?>
                                    <div id="permission-tree">
                                        <ul>
                                            <?php foreach($groups as $group): ?>
                                                <li><?php echo $group->name; ?>

                                                    <?php if($group->permissions->count()): ?>
                                                        <ul>
                                                            <?php foreach($group->permissions as $permission): ?>
                                                                <li id="<?php echo $permission->id; ?>"
                                                                    data-dependencies="<?php echo json_encode($permission->dependencies->lists('dependency_id')->all()); ?>">

                                                                    <?php if($permission->dependencies->count()): ?>
                                                                        <?php
                                                                        //Get the dependency list for the tooltip
                                                                        $dependency_list = [];
                                                                        foreach ($permission->dependencies as $dependency)
                                                                            array_push($dependency_list, $dependency->permission->name);
                                                                        $dependency_list = implode(", ", $dependency_list);
                                                                        ?>
                                                                        <a data-toggle="tooltip" data-html="true"
                                                                           title="<strong>Dependencies:</strong> <?php echo $dependency_list; ?>"><?php echo $permission->name; ?>

                                                                            <small><strong>(D)</strong></small>
                                                                        </a>
                                                                    <?php else: ?>
                                                                        <?php echo $permission->name; ?>

                                                                    <?php endif; ?>

                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>

                                                    <?php if($group->children->count()): ?>
                                                        <ul>
                                                            <?php foreach($group->children as $child): ?>
                                                                <li><?php echo $child->name; ?>

                                                                    <?php if($child->permissions->count()): ?>
                                                                        <ul style="padding-left:40px;font-size:.8em">
                                                                            <?php foreach($child->permissions as $permission): ?>
                                                                                <li id="<?php echo $permission->id; ?>"
                                                                                    data-dependencies="<?php echo json_encode($permission->dependencies->lists('dependency_id')->all()); ?>">

                                                                                    <?php if($permission->dependencies->count()): ?>
                                                                                        <?php
                                                                                        //Get the dependency list for the tooltip
                                                                                        $dependency_list = [];
                                                                                        foreach ($permission->dependencies as $dependency)
                                                                                            array_push($dependency_list, $dependency->permission->name);
                                                                                        $dependency_list = implode(", ", $dependency_list);
                                                                                        ?>
                                                                                        <a data-toggle="tooltip"
                                                                                           data-html="true"
                                                                                           title="<strong>Dependencies:</strong> <?php echo $dependency_list; ?>"><?php echo $permission->name; ?>

                                                                                            <small><strong>(#)</strong></small>
                                                                                        </a>
                                                                                    <?php else: ?>
                                                                                        <?php echo $permission->name; ?>

                                                                                    <?php endif; ?>

                                                                                </li>
                                                                            <?php endforeach; ?>
                                                                        </ul>
                                                                    <?php endif; ?>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php else: ?>
                                    <p>There are no permission groups.</p>
                                <?php endif; ?>
                            </div>

                            <div class="col-lg-6">
                                <p><strong>Ungrouped Permissions</strong></p>

                                <?php if($permissions->count()): ?>
                                    <?php foreach($permissions as $perm): ?>
                                        <input type="checkbox" name="ungrouped[]" value="<?php echo $perm->id; ?>"
                                               id="perm_<?php echo $perm->id; ?>"
                                               <?php echo e(in_array($perm->id, $role_permissions) ? 'checked' : ""); ?> data-dependencies="<?php echo json_encode($perm->dependencies->lists('dependency_id')->all()); ?>"/>
                                        <label for="perm_<?php echo $perm->id; ?>">

                                            <?php if($perm->dependencies->count()): ?>
                                                <?php
                                                //Get the dependency list for the tooltip
                                                $dependency_list = [];
                                                foreach ($perm->dependencies as $dependency)
                                                    array_push($dependency_list, $dependency->permission->name);
                                                $dependency_list = implode(", ", $dependency_list);
                                                ?>
                                                <a style="color:black;text-decoration:none;" data-toggle="tooltip"
                                                   data-html="true"
                                                   title="<strong>Dependencies:</strong> <?php echo $dependency_list; ?>"><?php echo $perm->name; ?>

                                                    <small><strong>(D)</strong></small>
                                                </a>
                                            <?php else: ?>
                                                <?php echo $perm->name; ?>

                                            <?php endif; ?>

                                        </label><br/>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <p>There are no ungrouped permissions.</p>
                                <?php endif; ?>
                            </div><!--col-lg-6-->
                        </div><!--row-->
                    </div><!--available permissions-->
                </div><!--col-lg-3-->
            </div><!--form control-->
            <div class="pull-right">
                <input type="submit" class="btn btn-success" value="Update"/>
                <a href="<?php echo route('dashboard.roles.index'); ?>" class="btn btn-danger">Cancel</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <?php echo Form::hidden('permissions'); ?>

    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('after-scripts-end'); ?>
    <?php echo Html::script('assets/js/jstree.min.js'); ?>

    <?php echo Html::script('assets/js/acl/role/script.js'); ?>


    <script>
        $(function() {
            <?php foreach($role_permissions as $permission): ?>
                tree.jstree('check_node', '#<?php echo $permission; ?>');
            <?php endforeach; ?>
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>