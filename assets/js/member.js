$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


//according menu

$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#curr_country").change(function () {
        toggleFields();
    });

    $("#payment_modes").change(function () {
        toggleFields();
    });

    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');

    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});

    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');

    // The Accordion Effect
    $('.accordion-header').click(function () {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }

        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    return false;
});

$(document).ready(function () {
    $('#membersForm').validate({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        // This option will not ignore invisible fields which belong to inactive panels
        excluded: ':disabled',
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The First Name is required'
                    }
                }
            }
        }
    });
});

function toggleFields() {
    if ($("#curr_country").val() == '0') {
        $("#curr_district").prop('required', true);
        $("#currVDC").prop('required', true);
        $("#curr_ward").prop('required', true);
        $("#state").prop('required', false);
        $("#zip_or_postal").prop('required', false);
        $("#city").prop('required', false);
        $("#street").prop('required', false);
        $("#house_number").prop('required', false);
        $("#curr_district").show();
        $("#currVDC").show();
        $("#curr_ward").show();
        $("#tole").show();
        $("#state").hide();
        $("#zip_or_postal").hide();
        $("#city").hide();
        $("#street").hide();
        $("#house_number").hide();
    }
    else {
        $("#curr_district").prop('required', false);
        $("#currVDC").prop('required', false);
        $("#curr_ward").prop('required', false);
        $("#state").prop('required', false);
        $("#zip_or_postal").prop('required', false);
        $("#city").prop('required', false);
        $("#street").prop('required', false);
        $("#house_number").prop('required', false);
        $("#curr_district").hide();
        $("#currVDC").hide();
        $("#curr_ward").hide();
        $("#tole").hide();
        $("#state").show();
        $("#zip_or_postal").show();
        $("#city").show();
        $("#street").show();
        $("#house_number").show();
    }
    if ($("#payment_modes").val() == 'cheque') {
        $("#cheque_number").prop('required', true);
        $("#cheque").show();
    }
    else {
        $("#cheque_number").prop('required', false);
        $("#cheque").hide();
    }
}
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
});
