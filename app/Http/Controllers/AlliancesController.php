<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Requests\AllianceRequest;
use App\Http\Requests\CreateAllianceRequest;
use App\Models\Dashboard\Alliance;
use App\Repositories\Dashboard\Alliance\IAllianceRepositoryContract;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlliancesController extends Controller
{
    /**
     * @var IAllianceRepositoryContract
     */
    protected $alliance;

    /**
     * AllianceController constructor.
     * @param IAllianceRepositoryContract $alliances
     */
    public function __construct(
        IAllianceRepositoryContract $alliances
    )
    {
        $this->alliance = $alliances;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($search = $request->input('search')) {

        } else {
            $alliances = Alliance::all();
        }

        return view('pages.alliances.index', compact('alliances', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CreateAllianceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreateAllianceRequest $request)
    {
        $submitText = ' Add ';

        $alliance = new Alliance();

        return view('pages.alliances.create', compact('alliance', 'submitText'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AllianceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllianceRequest $request)
    {
        $this->alliance->create($request->all());

        return redirect()->route('dashboard.alliances.index')->withFlashSuccess(trans('alerts.dashboard.alliance.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alliance = $this->alliance->findOrThrowException($id);

        return view('pages.alliances.show', compact('alliance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submitText = ' Update ';

        $alliance = $this->alliance->findOrThrowException($id, true);

        return view('pages.alliances.edit', compact('alliance', 'submitText'))
            ->withAlliance($alliance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->alliance->update($id, $request->all());

        return redirect()->route('dashboard.alliances.index')->withFlashSuccess(trans('alerts.dashboard.alliances.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Alliance::withTrashed()->findOrFail($id)->delete();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Alliance::withTrashed()->findOrFail($id)->forceDelete();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function deleted()
    {
        $alliances = Alliance::onlyTrashed()->paginate(50);

        return view('pages.alliances.deleted', compact('alliances', 'search'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @internal param RestoreUserRequest $request
     */
    public function restore($id)
    {
        $member = Alliance::withTrashed()->find($id);

        if ($member->restore()) {
            return redirect()->back()->withFlashSuccess(trans('alerts.dashboard.alliances.restored'));
        }

        throw new GeneralException(trans('exceptions.dashboard.access.alliances.restore_error'));
    }
}
