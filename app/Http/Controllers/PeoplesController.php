<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Requests\PeoplesRequest;
use App\Http\Utilities\NepaliDate;
use App\Models\Dashboard\People;
use App\Repositories\Dashboard\People\IPeopleRepositoryContract;
use Illuminate\Http\Request;

class PeoplesController extends Controller
{
    private $people;

    /**
     * Function : __constructor
     * Function for Constructor
     * @param IPeopleRepositoryContract $people
     * @internal param IPhotoRepositoryContract $photoRepo
     */
    public function __construct(
        IPeopleRepositoryContract $people
    )
    {
        /**
         * This will call for authentication before allowing access
         * over other this module.
         */
        $this->middleware('acl.routeNeedsPermission:manage-peoples');
        $this->people = $people;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.peoples.index')
            ->withPeoples($this->people->getAllPeoples());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $people = new People();

        return view('pages.peoples.create', compact('people'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        $this->people->create($request);

        return redirect()
            ->route('dashboard.peoples.index')
            ->withFlashSuccess(trans('alerts.admin.peoples.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param People $people
     * @internal param int $id
     */
    public function show($id)
    {
        return view('pages.peoples.show')
            ->withPeople($this->people->findOrThrowException($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit($id)
    {
        return view('pages.peoples.edit')
            ->withPeople($this->people->findOrThrowException($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PeoplesRequest|Request $request
     * @param People $people
     * @return \Illuminate\Http\Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function update(Request $request, People $people)
    {
        if ($people->update($request->all())) {
            $people->save();

            return redirect()
                ->route('dashboard.peoples.index')
                ->withFlashSuccess(trans('alerts.admin.peoples.updated'));
        }

        throw new GeneralException();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param People $people
     * @internal param int $id
     */
    public function destroy($id)
    {
        $this->people->destroy($id);

        return redirect()
            ->back()
            ->withFlashSuccess(trans('alerts.admin.people.deleted'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param People $people
     * @internal param int $id
     */
    public
    function delete($id)
    {
        $this->people->delete($id);

        return redirect()
            ->back()
            ->withFlashSuccess(trans('alerts.admin.people.deleted_permanently'));;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function deleted(Request $request)
    {
        return view('pages.peoples.deleted')
            ->withPeoples($this->people->getAllPeoples());
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @internal param RestoreUserRequest $request
     */
    public function restore($id)
    {
        $this->people->restore($id);

        return redirect()
            ->back()
            ->withFlashSuccess(trans('alerts.admin.peoples.restored'));
    }

    /**
     * @param $id
     * @param $disabled_status
     * @return mixed
     * @throws GeneralException
     * @internal param MarkUserRequest $request
     */
    public function mark($id, $disabled_status)
    {
        $people = People::find($id);

        $people->disabled_status = $disabled_status;

        if ($people->save()) {
            return redirect()->back();
        }

        throw new GeneralException(trans('exceptions.dashboard.peoples.mark_error'));

    }


    public function make_member(People $people)
    {
        if (!in_array($people->membership_type, array('A', 'G'))) {
            $nepDate = new NepaliDate();
            return view('pages.peoples.upgrade', compact('people', 'nepDate'));

        }

        return redirect()
            ->route('dashboard.peoples.index')
            ->withFlashSuccess(trans('alerts.admin.peoples.upgraded'));
    }

    public function upgrade(Request $request, People $people)
    {
        $this->people->upgrade_to_member($request, $people);

        return redirect()
            ->route('dashboard.peoples.index')
            ->withFlashSuccess(trans('alerts.admin.peoples.upgraded'));

    }
}