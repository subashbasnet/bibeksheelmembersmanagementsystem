<?php

namespace App\Http\Controllers\ACL\Role;

use App\Http\Controllers\Controller;
use App\Repositories\ACL\Role\RoleRepositoryContract;
use App\Http\Requests\ACL\Role\EditRoleRequest;
use App\Http\Requests\ACL\Role\StoreRoleRequest;
use App\Http\Requests\ACL\Role\CreateRoleRequest;
use App\Http\Requests\ACL\Role\DeleteRoleRequest;
use App\Http\Requests\ACL\Role\UpdateRoleRequest;
use App\Repositories\ACL\Permission\PermissionRepositoryContract;
use App\Repositories\ACL\Permission\Group\PermissionGroupRepositoryContract;

/**
 * Class RoleController
 * @package App\Http\Controllers\ACL
 */
class RoleController extends Controller
{
    /**
     * @var RoleRepositoryContract
     */
    protected $roles;

    /**
     * @var PermissionRepositoryContract
     */
    protected $permissions;

    /**
     * @param RoleRepositoryContract       $roles
     * @param PermissionRepositoryContract $permissions
     */
    public function __construct(
        RoleRepositoryContract $roles,
        PermissionRepositoryContract $permissions
    )
    {
        $this->roles       = $roles;
        $this->permissions = $permissions;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('dashboard.acl.roles.index')
            ->withRoles($this->roles->getRolesPaginated(50));
    }

    /**
     * @param  PermissionGroupRepositoryContract $group
     * @param  CreateRoleRequest                 $request
     * @return mixed
     */
    public function create(PermissionGroupRepositoryContract $group, CreateRoleRequest $request)
    {
        return view('dashboard.acl.roles.create')
            ->withGroups($group->getAllGroups())
            ->withPermissions($this->permissions->getUngroupedPermissions());
    }

    /**
     * @param  StoreRoleRequest $request
     * @return mixed
     */
    public function store(StoreRoleRequest $request)
    {
        $this->roles->create($request->all());
        return redirect()->route('dashboard.roles.index')
            ->withFlashSuccess(trans('alerts.dashboard.roles.created'));
    }

    /**
     * @param  $id
     * @param  PermissionGroupRepositoryContract $group
     * @param  EditRoleRequest                   $request
     * @return mixed
     */
    public function edit($id, PermissionGroupRepositoryContract $group, EditRoleRequest $request)
    {
        $role = $this->roles->findOrThrowException($id, true);
        return view('dashboard.acl.roles.edit')
            ->withRole($role)
            ->withRolePermissions($role->permissions->lists('id')->all())
            ->withGroups($group->getAllGroups())
            ->withPermissions($this->permissions->getUngroupedPermissions());
    }

    /**
     * @param  $id
     * @param  UpdateRoleRequest $request
     * @return mixed
     */
    public function update($id, UpdateRoleRequest $request)
    {
        $this->roles->update($id, $request->all());
        return redirect()->route('dashboard.roles.index')->withFlashSuccess(trans('alerts.dashboard.roles.updated'));
    }

    /**
     * @param  $id
     * @param  DeleteRoleRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteRoleRequest $request)
    {
        $this->roles->destroy($id);
        return redirect()->route('dashboard.roles.index')->withFlashSuccess(trans('alerts.dashboard.roles.deleted'));
    }
}
