<?php

namespace App\Http\Controllers;

use App\Events\Event;
use App\Http\Controllers\Controller;
use App\Models\Dashboard\Alliance;
use App\Models\Dashboard\CalendarEvent;
use App\Models\Dashboard\Member;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $count['members'] = Member::with('membership')
            ->whereIn('membership_type', array('A','G'))
            ->where('disabled_status', 1)
            ->where('membership_status', 1)
            ->count();

        $count['events'] = CalendarEvent::where('end','>=',Carbon::now())
            ->where('disabled_status',1)
            ->count();

        $count['alliances'] = Alliance::count();

        return view('dashboard.index', compact('count'));
    }

    public function email()
    {
        Mail::raw('Hello Rajan', function($message)
        {
            $message->from('info@thinktanknepal.com');
            $message->to('rajan.adhikari99@gmail.com');
            $message->subject('Test Email');
        });
    }
}