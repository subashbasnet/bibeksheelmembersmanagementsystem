<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Http\Requests\MembersRequest;
use App\Http\Utilities\NepaliDate;
use App\Models\Dashboard\Member;
use App\Models\Dashboard\Membership;
use App\Repositories\Dashboard\Member\IMemberRepositoryContract;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;

class MembersController extends Controller
{
    private $memberRepo;

    /**
     * Function : __constructor
     * Function for Constructor
     * @param IMemberRepositoryContract $memberRepo
     * @internal param IPhotoRepositoryContract $photoRepo
     */
    public function __construct(
        IMemberRepositoryContract $memberRepo
    )
    {
        /**
         * This will call for authentication before allowing access
         * over other this module.
         */
        $this->middleware('acl.routeNeedsPermission:manage-members');
        $this->memberRepo = $memberRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $members = Member::with('membership')
            ->whereIn('membership_type', array('A', 'G'))
            ->where('disabled_status', 1)
            ->where('membership_status', 1)
            ->get();

        return view('pages.members.index', compact('members', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $submitText = ' Add ';

        $member = new Member();

        $nepDate = new NepaliDate();

        return view('pages.members.create', compact('member', 'submitText', 'nepDate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        $this->memberRepo->create($request);

        return redirect()
            ->route('dashboard.members.index')
            ->withFlashSuccess('Great! New Member Added.');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Member $member
     * @internal param int $id
     */
    public function show($id)
    {
        $member = Member::with('membership')->find($id);

        return view('pages.members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit($id)
    {
        $member = Member::with('membership')->findorfail($id);

        $submitText = ' Update ';

        return view('pages.members.edit', compact('member', 'submitText'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MembersRequest|Request $request
     * @param Member $member
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Member $member)
    {
        $membership = $member->membership;
        $data = $request->all();

        // Update Membership Data
        $membership->registration_date = $data['registration_date'];
        $membership->renewal_date = $data['renewal_date'];
        $membership->membership_type = $data['membership_type'];
        $membership->document_number = $data['document_number'];
        $membership->verified_document = $data['verified_document'];
        $membership->save();

        $member->update($request->all());

        $this->memberRepo->resetValues($member);
        $member->save();

        return redirect()
            ->route('dashboard.members.index')
            ->withFlashSuccess('Information Update has been successful.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Member $member
     * @internal param int $id
     */
    public function destroy($id)
    {
        Member::withTrashed()->findOrFail($id)->delete();

        return redirect()
            ->back()
            ->withFlashSuccess('Members Deleted successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Member $member
     * @internal param int $id
     */
    public function delete($id)
    {
        Member::withTrashed()->findOrFail($id)->forceDelete();

        return redirect()->back();
    }

    public function expired(Request $request)
    {
        $members = Member::with('membership')->where('membership_status', 0)->SimplePaginate(50);

        return view('pages.members.inactive', compact('members', 'search'));
    }

    public function disabled(Request $request)
    {
        $members = Member::with('membership')->where('disabled_status', 0)->SimplePaginate(50);

        return view('pages.members.disabled', compact('members', 'search'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function deleted(Request $request)
    {
        $members = Member::onlyTrashed()->with('membership')->SimplePaginate(50);

        return view('pages.members.deleted', compact('members', 'search'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @internal param RestoreUserRequest $request
     */
    public function restore($id)
    {
        $member = Member::withTrashed()->find($id);

        if ($member->restore()) {
            return redirect()->back()->withFlashSuccess(trans('alerts.dashboard.users.restored'));
        }

        throw new GeneralException(trans('exceptions.dashboard.access.users.restore_error'));
    }

    /**
     * @param $id
     * @param $disabled_status
     * @return mixed
     * @throws GeneralException
     * @internal param MarkUserRequest $request
     */
    public function mark($id, $disabled_status)
    {
        $member = Member::find($id);

        $member->disabled_status = $disabled_status;

        if ($member->save()) {
            return redirect()->back();
        }

        throw new GeneralException(trans('exceptions.dashboard.members.mark_error'));

    }
}