<?php

namespace App\Http\Controllers;

use App\Models\Dashboard\MailBox;
use App\Repositories\Dashboard\MailBox\IMailBoxRepositoryContract;
use Illuminate\Http\Request;

use App\Http\Requests;

class MailBoxController extends Controller
{
    protected $mailbox;

    /**
     * MailBoxController constructor.
     * @param IMailBoxRepositoryContract $mailBox
     */
//    public function __construct(IMailBoxRepositoryContract $mailBox)
//    {
//        $this->mailbox = $mailBox;
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mails = MailBox::all();

        return view('pages.email.mailbox',compact('mails'));
    }

    public function compose()
    {
        return view('pages.email.compose');
    }

    public function read($id)
    {
        $mail = MailBox::findorfail($id);

        return view('pages.email.read', compact('mail'));
    }
}
