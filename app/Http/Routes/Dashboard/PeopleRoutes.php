<?php

Route::group(['middleware' => 'web'], function () {

    Route::resource('peoples', 'PeoplesController');

    Route::get('people/{people}/payment/create', 'PeoplePaymentsController@create')->name('dashboard.people.payment.create');

    Route::get('people/{people}/payment', 'PeoplePaymentsController@index')->name('dashboard.people.payment.index');

    Route::get('people/{people}/make_member', 'PeoplesController@make_member')->name('dashboard.peoples.make_member');

    Route::post('people/{people}/payment', 'PeoplePaymentsController@store')->name('dashboard.people.payment.store');

    Route::delete('people/{people}/payment/{payment}', 'PeoplePaymentsController@destroy')->name('dashboard.people.payment.destroy');

    Route::get('people/{people}/payment/{payment}/edit', 'PeoplePaymentsController@edit')->name('dashboard.people.payment.edit');

    Route::put('people/{people}/payment/{payment}', 'PeoplePaymentsController@update')->name('dashboard.people.payment.update');

    Route::put('peoples/{people}/update', 'PeoplesController@update')->name('dashboard.peoples.update');

    Route::put('peoples/{people}/upgrade', 'PeoplesController@upgrade')->name('dashboard.peoples.upgrade');

    Route::get('district/{district}/vdcs.json',function($district) {
        $vdcs =\App\Models\Dashboard\Abode::where('district',$district)->lists('vdc');
        foreach($vdcs as $vdc) {
            echo '<option value="' . $vdc . '">' . $vdc . '</option>';
        }
    })->name('dashboard.abodes');
});
