<?php

Route::group(['middleware' => 'web'], function () {

    Route::get('events/deleted', 'CalendarEventsController@deleted')->name('dashboard.events.deleted');

    Route::get('events/canceled', 'CalendarEventsController@canceled')->name('dashboard.events.canceled');

    Route::get('events/{events}/mark/{status}', 'CalendarEventsController@mark')->name('dashboard.events.mark')->where(['status' => '[0,1]']);

    Route::get('events/calendar', 'CalendarEventsController@calendar')->name('dashboard.events.calendar');

    Route::get('events/{member}/restore', 'CalendarEventsController@restore')->name('dashboard.events.restore');

    Route::resource('events', 'CalendarEventsController');

    Route::get('events/{member}/delete', 'CalendarEventsController@delete')->name('dashboard.events.delete-permanently');
});
