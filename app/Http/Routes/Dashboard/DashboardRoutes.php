<?php

Route::get('/', 'HomeController@index')->name('admin.home');

Route::get('/admin/setting/config', 'JugalAdminController@config')->name('admin.setting.config');

Route::post('/admin/setting/store', 'JugalAdminController@store')->name('admin.setting.store');

Route::get('/admin/setting/app_config', 'JugalAdminController@mobile_app_config')->name('admin.setting.mobile_config');

Route::get('/app_config', 'JugalAdminController@config_response');