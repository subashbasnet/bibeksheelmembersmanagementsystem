<?php

Route::group(['middleware' => 'web'], function () {

//    Route::get('members/inactive', 'MembersController@expire')->name('dashboard.members.inactive');

    Route::resource('members', 'MembersController');

    Route::get('member/{member}/payment/create', 'MemberPaymentsController@create')->name('dashboard.member.payment.create');

    Route::get('member/{member}/payment', 'MemberPaymentsController@index')->name('dashboard.member.payment.index');

    Route::post('member/{member}/payment', 'MemberPaymentsController@store')->name('dashboard.member.payment.store');

    Route::delete('member/{member}/payment/{payment}', 'MemberPaymentsController@destroy')->name('dashboard.member.payment.destroy');

    Route::get('member/{member}/payment/{payment}/edit', 'MemberPaymentsController@edit')->name('dashboard.member.payment.edit');

    Route::put('member/{member}/payment/{payment}', 'MemberPaymentsController@update')->name('dashboard.member.payment.update');

    Route::put('members/{member}', 'MembersController@update')->name('dashboard.member.update');

    Route::get('district/{district}/vdcs.json',function($district) {
        $vdcs =\App\Models\Dashboard\Abode::where('district',$district)->lists('vdc');
        foreach($vdcs as $vdc) {
                echo '<option value="' . $vdc . '">' . $vdc . '</option>';
        }
    })->name('dashboard.abodes');
});
