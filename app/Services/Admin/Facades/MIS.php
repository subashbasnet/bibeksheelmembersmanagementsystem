<?php
namespace App\Services\Admin;

use Illuminate\Support\Facades\Facade;

class MIS extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mis';
    }
}