<?php

namespace App\Services\ACL\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class ACL
 * @package App\Services\ACL\Facades
 */
class ACL extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'acl';
    }
}