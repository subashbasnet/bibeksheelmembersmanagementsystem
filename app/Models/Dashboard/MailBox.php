<?php

namespace App\Models\Dashboard;

use Illuminate\Database\Eloquent\Model;

class MailBox extends Model
{
    protected $table = 'mailboxes';

    protected $fillable = [
         'sender'
        ,'to'
        ,'cc'
        ,'bcc'
        ,'reply_to'
        ,'subject'
        ,'priority'
        ,'body'
        ,'attachment'
        ,'schedule_date'
        ,'status'
    ];
}
