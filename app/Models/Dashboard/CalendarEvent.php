<?php

namespace App\Models\Dashboard;

use App\Exceptions\GeneralException;
use App\Models\Dashboard\Traits\EventAttribute;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use MaddHatter\LaravelFullcalendar\Event;
use MaddHatter\LaravelFullcalendar\IdentifiableEvent;

class CalendarEvent extends Model implements Event
{
    use EventAttribute;

    use SoftDeletes;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attribute is for deleted record, events start date and events end date
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'start', 'end'];

//    protected $dates = [ 'start', 'end'];


    protected $fillable = [

    ];

    /**
     * Get the events's id number
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the events's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day events?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Optional FullCalendar.io settings for this events
     *
     * @return array
     */
    public function getEventOptions()
    {
        return [
            'color' => $this->background_color
        ];
    }

    public function scopeUpcomingEvents($query)
    {
        $query->where('disabled_status',1)->where('end','>=', Carbon::now());

    }
}
