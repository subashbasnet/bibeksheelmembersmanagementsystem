<?php

namespace App\Models\Dashboard;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * @property mixed registration_date
 */
class Membership extends Model
{
    private $status;

    protected $fillable = [
         'member_id'
        ,'receipt_id'
        ,'registration_date'
        ,'renewal_date'
        ,'membership_year'
        ,'membership_type'
        ,'verified_document'
        ,'document_number'
        ,'document_path'
    ];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function show_registration_date()
    {
        return $this->registration_date;
    }

    public function showMembershipStatus()
    {
        $renew = (new Carbon($this->renewal_date))->addYear(1);

        $now = Carbon::now();

        return ($renew->lt($now)) ? 'Expired': 'Current';
    }

    public function getExpiryDate()
    {
        $renew = new Carbon($this->renewal_date - 365);

        return ($renew->lt($now)) ? 'Expired': 'Current';
    }
}
