<?php

namespace App\Models\Dashboard\Traits;


trait MemberAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('edit-members')) {
            return '<a href="' . route('dashboard.members.edit', $this->id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
            if (access()->allow('delete-members')) {
                return '<a href="' . route('dashboard.members.destroy', $this->id) . '" class="btn btn-xs btn-danger" data-method="delete"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
            }

        return '';
    }

    public function getPaymentButtonAttribute()
    {
            if (access()->allow('view-payments')) {
                return '<a href="' . route('dashboard.member.payment.index', $this->id) . '" class="btn btn-xs btn-success"><i class="fa fa-dollar" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.payment') . '"></i></a>';
            }

        return '';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->disabled_status) {
            case 0:
                if (access()->allow('reactivate-members')) {
                    return '<a href="' . route('dashboard.member.mark', [$this->id, 1]) . '" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.dashboard.members.activate') . '"></i></a> ';
                }

                break;

            case 1:
                if (access()->allow('deactivate-members')) {
                    return '<a href="' . route('dashboard.member.mark', [$this->id, 0]) . '" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.dashboard.members.deactivate') . '"></i></a> ';
                }

                break;

            default:
                return '';
            // No break
        }

        return '';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->getPaymentButtonAttribute() .' '
            .$this->getStatusButtonAttribute()
            . $this->getEditButtonAttribute()
            . $this->getDeleteButtonAttribute();
    }
}