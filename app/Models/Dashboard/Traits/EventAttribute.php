<?php
/**
 * Created by PhpStorm.
 * User: rajan
 * Date: 12/19/2016
 * Time: 11:29 AM
 */

namespace App\Models\Dashboard\Traits;


trait EventAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('edit-events')) {
            return '<a href="' . route('dashboard.events.edit', $this->id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (access()->allow('delete-events')) {
            return '<a href="' . route('dashboard.events.destroy', $this->id) . '" class="btn btn-xs btn-danger" data-method="delete"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->disabled_status) {
            case 0:
                if (access()->allow('reactivate-events')) {
                    return '<a href="' . route('dashboard.events.mark', [$this->id, 1]) . '" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.dashboard.events.activate') . '"></i></a> ';
                }

                break;

            case 1:
                if (access()->allow('deactivate-events')) {
                    return '<a href="' . route('dashboard.events.mark', [$this->id, 0]) . '" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.dashboard.events.deactivate') . '"></i></a> ';
                }

                break;

            default:
                return '';
            // No break
        }

        return '';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getEditButtonAttribute()
                .$this->getStatusButtonAttribute()
            . $this->getDeleteButtonAttribute();
    }
}