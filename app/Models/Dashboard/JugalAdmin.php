<?php
namespace App\Models\Dashboard;

use Illuminate\Database\Eloquent\Model;

class JugalAdmin extends Model
{
    /**
     * @var string
     */
    protected $table = 'mis_config';

    /**
     * @var array
     */
    protected $fillable = [
        "key", "value", "section"
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @param $key
     * @return bool
     */
    public static function getByKey($key) {
        $row = JugalAdmin::where('key',$key)->first();
        if(isset($row->value)) {
            return $row->value;
        } else {
            return false;
        }
    }

    /**
     * @return object
     */
    public static function getAll() {
        $configs = array();
        $configs_db = JugalAdmin::all();
        foreach ($configs_db as $row) {
            $configs[$row->key] = $row->value;
        }
        return (object) $configs;
    }

}
