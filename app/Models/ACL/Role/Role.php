<?php

namespace App\Models\ACL\Role;

use Illuminate\Database\Eloquent\Model;
use App\Models\ACL\Role\Traits\RoleAccess;
use App\Models\ACL\Role\Traits\Attribute\RoleAttribute;

/**
 * Class Role
 * @package App\Models\ACL\Role
 */
class Role extends Model
{
    use RoleAccess, RoleAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('acl.role_table');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(config('acl.user_model'), config('acl.user_role_pivot_table'), 'role_id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(config('acl.permission_model'), config('acl.permission_role_pivot_table'), 'role_id', 'permission_id')
            ->orderBy('slug', 'asc');
    }
}
