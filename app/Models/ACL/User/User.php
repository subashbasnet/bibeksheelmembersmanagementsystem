<?php

namespace App\Models\ACL\User;

use App\Models\ACL\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\ACL\User\Traits\Attribute\UserAttribute;

/**
 * Class User
 * @package App\Models\ACL\User
 */
class User extends Authenticatable
{
    use SoftDeletes, UserAccess, UserAttribute;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('acl.role_model'), config('acl.role_user_pivot_table'), 'user_id', 'role_id');
    }
}
