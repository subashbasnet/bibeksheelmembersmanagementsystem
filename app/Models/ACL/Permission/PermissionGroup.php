<?php

namespace App\Models\ACL\Permission;

use Illuminate\Database\Eloquent\Model;
use App\Models\ACL\Permission\Traits\Attribute\PermissionGroupAttribute;

/**
 * Class PermissionGroup
 * @package App\Models\ACL\Permission
 */
class PermissionGroup extends Model
{
    use  PermissionGroupAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('acl.group_table');
    }

    /**
     * @return mixed
     */
    public function children()
    {
        return $this->hasMany(config('acl.group_model'), 'parent_id', 'id')->orderBy('sort', 'asc');
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->hasMany(config('acl.permission_model'), 'group_id')->orderBy('sort', 'asc');
    }
}