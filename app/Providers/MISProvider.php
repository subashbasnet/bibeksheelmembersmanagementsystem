<?php

namespace App\Providers;

use App\Models\Dashboard\JugalAdmin;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class MISProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerFacade();
        $this->registerBindings();
        $this->registerAlias();
    }

    private function registerFacade()
    {
        $this->app->booting(function () {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('ACL', \App\Services\Admin\MIS::class);
        });
    }

    private function registerBindings()
    {
        $this->app->bind(
            \App\Repositories\Dashboard\Member\IMemberRepositoryContract::class,
            \App\Repositories\Dashboard\Member\MemberRepository::class
        );
        $this->app->bind(
            \App\Repositories\Dashboard\People\IPeopleRepositoryContract::class,
            \App\Repositories\Dashboard\People\PeopleRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Membership\IMembershipRepositoryContract::class,
            \App\Repositories\Dashboard\Membership\MembershipRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Payments\IPaymentRepositoryContract::class,
            \App\Repositories\Dashboard\Payments\PaymentRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Photo\IPhotoRepositoryContract::class,
            \App\Repositories\Dashboard\Photo\PhotoRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Alliance\IAllianceRepositoryContract::class,
            \App\Repositories\Dashboard\Alliance\AllianceRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\CalendarEvent\ICalendarEventRepositoryContract::class,
            \App\Repositories\Dashboard\CalendarEvent\CalendarEventRepository::class
        );

        $this->app->bind(
            \App\Repositories\Admin\IJugalAdminRepositoryContract::class,
            \App\Repositories\Admin\JugalAdminRepository::class
        );
    }

    private function registerAlias()
    {
        $loader = AliasLoader::getInstance();

        $loader->alias('JugalAdmin', JugalAdmin::class);
    }
}
