<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\Dashboard\Member\IMemberRepositoryContract::class,
            \App\Repositories\Dashboard\Member\MemberRepository::class
        );
        $this->app->bind(
            \App\Repositories\Dashboard\People\IPeopleRepositoryContract::class,
            \App\Repositories\Dashboard\People\PeopleRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Membership\IMembershipRepositoryContract::class,
            \App\Repositories\Dashboard\Membership\MembershipRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Payments\IPaymentRepositoryContract::class,
            \App\Repositories\Dashboard\Payments\PaymentRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Photo\IPhotoRepositoryContract::class,
            \App\Repositories\Dashboard\Photo\PhotoRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\Alliance\IAllianceRepositoryContract::class,
            \App\Repositories\Dashboard\Alliance\AllianceRepository::class
        );

        $this->app->bind(
            \App\Repositories\Dashboard\CalendarEvent\ICalendarEventRepositoryContract::class,
            \App\Repositories\Dashboard\CalendarEvent\CalendarEventRepository::class
        );

        $this->app->bind(
            \App\Repositories\Admin\IJugalAdminRepositoryContract::class,
            \App\Repositories\Admin\JugalAdminRepository::class
        );
    }
}
