<?php
/**
 * Created by PhpStorm.
 * User: rajan
 * Date: 1/7/2017
 * Time: 10:11 PM
 */

namespace App\Repositories\Dashboard\CalendarEvent;


interface ICalendarEventRepositoryContract
{
    /**
     * @param  $id
     * @return mixed
     */
    public function findOrThrowException($id);

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getEventPaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc');

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAllEvents($order_by = 'id', $sort = 'asc');

    /**
     * @param AllianceRequest $request
     * @return
     */
    public function create($request);


    public function getUpcomingEvents();


    public function getCanceledEvents();


    public function getCompletedEvents();
}