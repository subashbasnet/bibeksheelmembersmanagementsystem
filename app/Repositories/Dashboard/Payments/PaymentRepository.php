<?php

namespace App\Repositories\Dashboard\Payments;


use App\Exceptions\GeneralException;
use App\Models\Dashboard\Payment;
use Carbon\Carbon;

class  PaymentRepository implements IPaymentRepositoryContract
{
    private $payment;

    /**
     * PaymentRepository constructor.
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        if (! is_null(Payment::find($id))) {
            return Payment::findorfail($id);
        }

        throw new GeneralException('No Payment found');
    }

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getPaymentsPaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc')
    {
        return Payment::orderBy($order_by, $sort)
            ->paginate($per_page);
    }

    /**
     * @param null $member_id
     * @param bool $latest
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAllPayments($member_id = NULL, $latest = false, $order_by = 'id', $sort = 'asc')
    {
        if($member_id != NULL) {
            if($latest){
                return Payment::where('member_id', $member_id)
                    ->latest()
                    ->get();
            }
            return Payment::where('member_id', $member_id)
                ->orderBy($order_by, $sort)
                ->get();
        }

        return Payment::orderBy($order_by, $sort)
            ->get();

    }

    public function create($request, $people)
    {
        Payment::create($request->all());

        if($request['purpose'] == 'Membership Fee')
        {
            $people->membership_status = 1;
            $people->membership->renewal_date = $this->setMembershipExpiryDate($request['membership_year']);
            $people->membership->membership_year = $request['membership_year'];
            $people->save();
            $people->membership->save();
        };
    }

    /**
     * @param $request
     * @return static
     */
    public function createFirstPayment($request)
    {
        return $this->payment->create($request->all());
    }

    private function setMembershipExpiryDate($membership_year)
    {
        $membership_year ++;
        return toEnglishDate($membership_year.'-'.'01-01');
    }
}

