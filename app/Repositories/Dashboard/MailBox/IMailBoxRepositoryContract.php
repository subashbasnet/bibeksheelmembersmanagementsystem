<?php
/**
 * Created by PhpStorm.
 * User: rajan
 * Date: 2/10/2017
 * Time: 9:48 PM
 */

namespace App\Repositories\Dashboard\MailBox;


interface IMailBoxRepositoryContract
{
    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function findOrThrowException($id);

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAllMails($order_by = 'id', $sort = 'asc');

    /**
     * @param $request
     * @return mixed
     */
    public function create($request);

}