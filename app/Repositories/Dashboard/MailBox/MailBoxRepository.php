<?php
/**
 * Created by PhpStorm.
 * User: rajan
 * Date: 2/10/2017
 * Time: 9:49 PM
 */

namespace App\Repositories\Dashboard\MailBox;


use App\Exceptions\GeneralException;
use App\Models\Dashboard\MailBox;

class MailBoxRepository implements IMailBoxRepositoryContract
{
    private $mailbox;

    public function __construct(MailBox $mailBox)
    {
        $this->mailbox = $mailBox;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        if(! is_null(MailBox::find($id))) {
            return MailBox::findorfail($id);
        }

        throw new GeneralException('No mails Found');
    }

    /**
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAllMails($order_by = 'id', $sort = 'asc')
    {
        dd('hello');
        return MailBox::orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param $request
     * @return mixed
     */
    public function create($request)
    {
        // TODO: Implement create() method.
    }
}