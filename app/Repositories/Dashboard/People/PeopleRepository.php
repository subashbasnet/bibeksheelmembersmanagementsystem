<?php

namespace App\Repositories\Dashboard\People;

use App\Exceptions\GeneralException;
use App\Models\Dashboard\People;
use App\Repositories\Dashboard\Membership\IMembershipRepositoryContract;
use App\Repositories\Dashboard\Payments\IPaymentRepositoryContract;
use App\Repositories\Dashboard\Photo\IPhotoRepositoryContract;
use Illuminate\Http\Request;

/**
 * Class EloquentPeopleRepository
 * @property IPhotoRepositoryContract photoRepo
 * @property IPaymentRepositoryContract payment
 * @package App\Repositories\People
 */
class PeopleRepository implements IPeopleRepositoryContract
{
    private $people;

    private $payment;

    private $membership;

    public function __construct(
        People $people,
        IMembershipRepositoryContract $membership,
        IPaymentRepositoryContract $payment
    )
    {
        $this->people = $people;
        $this->membership = $membership;
        $this->payment = $payment;
    }

    /**
     * @param $id
     * @param bool $withPayments
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws GeneralException
     * @internal param bool $withPeopleship
     */
    public function findOrThrowException($id, $withPayments = false)
    {
        if (! is_null(People::find($id))) {
             if ($withPayments) {
                return People::with('payments')
                    ->find($id);
            }

            return People::findorfail($id);
        }

        throw new GeneralException('Peoples not found');
    }

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getPeoplesPaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc')
    {
        return People::where('id','!=','1')
            ->orderBy($order_by, $sort)
            ->paginate($per_page);
    }

    /**
     * @param  string $order_by
     * @param  string $sort
     * @param bool $withPayments
     * @return mixed
     */
    public function getAllPeoples($order_by = 'id', $sort = 'asc', $withPayments = false)
    {
        if ($withPayments) {
            return People::with('payments')
                ->where('disabled_status', 1)
                ->orderBy($order_by, $sort)
                ->get();
        }

        return People::orderBy($order_by, $sort)
            ->get();
    }

    public function create(Request $input)
    {
        $newPeople = $this->createPeople($input);

        $people = $this->findOrThrowException($newPeople['id']);

        $people['registration_id'] = $people['id'];

        $people->save();
    }

    public function createWithPhoto(Request $input, $doc, $photo)
    {
        $people_id = $this->createPeopleWithPhoto($input, $photo);
        $people = People::findorfail($people_id['id']);
        // Unique Registration-ID
        $people->registration_id = $people_id['id'];
        // 'M' denotes People
        $people->people_type = 'M';
        $this->resetValues($people);
        $people->save();

        $input['people_id'] = $people_id['id'];

        $this->createPayment($input);
    }

    /**
     * Reset values of Current Address depending on the chosen country
     * @param People $people
     */
    public function resetValues(People $people) {
        // 0 is Nepal
        if ($people->curr_country == '0') {
            $people->state = '';
            $people->zip_or_postal = '';
            $people->city = '';
            $people->street = '';
            $people->house_number = '';
        } else {
            $people->curr_district = '';
            $people->curr_vdc = '';
            $people->curr_ward = '';
        }
    }


    /**
     * @param Request $input
     * @return static
     * @internal param $photo
     */
    protected function createPeople(Request $input)
    {
        return $this->people->create($input->all());
    }

    /**
     * @param $input
     */
    protected function createPayment($input)
    {
        $this->payment->create($input);
    }

    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function destroy($id)
    {
        $people = $this->findOrThrowException($id);
        if ($people->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.admin.peoples.delete_error'));
    }

    /**
     * @param  $id
     * @throws GeneralException
     * @return boolean|null
     */
    public function delete($id)
    {
        $people = $this->findOrThrowException($id, true);

        try {
            $people->forceDelete();
        } catch (\Exception $e) {
            throw new GeneralException($e->getMessage());
        }
    }

    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function restore($id)
    {
        $people = $this->findOrThrowException($id);

        if ($people->restore()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.admin.people.restore_error'));
    }

    public function upgrade_to_member($input, $people)
    {
        if ($people->update($input->all())) {
            $people->save();
        }

        $input['member_id'] = $people->id;
        $input['purpose'] = 'Membership Fee';

        if($this->payment->createFirstPayment($input)) {
            $this->membership->create($input);
        }
    }
}
