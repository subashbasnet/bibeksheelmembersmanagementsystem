<?php

namespace App\Repositories\Dashboard\People;


use App\Http\Requests\PeoplesRequest;
use Illuminate\Http\Request;
use App\Models\Dashboard\People;

interface IPeopleRepositoryContract
{
    /**
     * @param $id
     * @param bool $withPayments
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @internal param bool $withPeopleship
     */
    public function findOrThrowException($id, $withPayments = false);

    /**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getPeoplesPaginated($per_page, $status,  $order_by = 'id', $sort = 'asc');

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAllPeoples($order_by = 'id', $sort = 'asc', $withPayments = false);


    /**
     * @param Request $request
     * @return mixed
     * @internal param $doc
     */
    public function create(Request $request);

    public function resetValues(People $people);

    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function destroy($id);

    /**
     * @param  $id
     * @throws GeneralException
     * @return boolean|null
     */
    public function delete($id);

    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function restore($id);

    /**
     * @param $input
     * @param $people
     * @return mixed
     * @internal param $id
     */
    public function upgrade_to_member($input, $people);
}