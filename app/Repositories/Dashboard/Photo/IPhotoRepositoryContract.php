<?php

namespace App\Repositories\Dashboard\Photo;

use Illuminate\Http\UploadedFile;

interface IPhotoRepositoryContract
{

    function upload(UploadedFile $uploadedFile, $filePath);
    public function fileName();
}
