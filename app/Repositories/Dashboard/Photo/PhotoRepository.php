<?php

namespace App\Repositories\Dashboard\Photo;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class PhotoRepository implements IPhotoRepositoryContract
{
    protected $filePath;

    protected $file;

    public function getDocPath()
    {
        return $this->filePath;
    }

    /**
     * Functions generates filename for uploaded file
     * @return string
     */
    public function fileName()
    {
        $name = md5(
            $this->file->getClientOriginalName()
        );

        $extension = $this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

    /**
     * Function moves the uploaded file to given path and create the thumbnail
     *
     * @param UploadedFile $uploadedFile
     * @param String $filePath
     * @return $this
     */
    public function upload(UploadedFile $uploadedFile, $filePath)
    {
        $this->setFile($uploadedFile, $filePath);

        $file = $this->file->move($filePath,$this->fileName());

        $this->makeImage($file);

        return $this->filePath();
    }

    public function filePath()
    {
       return $this->getDocPath().$this->fileName();
    }

    public function makeImage($file)
    {
        $img = Image::make($this->getDocPath().$this->fileName())
            ->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            });

        $img->save($this->filePath());
    }

    public function setFile($uploadedFile, $filePath)
    {
        $this->file = $uploadedFile;

        $this->filePath = $filePath;
    }
}
