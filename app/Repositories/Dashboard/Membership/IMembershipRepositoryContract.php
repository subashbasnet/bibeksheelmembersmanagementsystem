<?php

namespace App\Repositories\Dashboard\Membership;


use Illuminate\Http\UploadedFile;

interface IMembershipRepositoryContract
{

    public function create($request);
}