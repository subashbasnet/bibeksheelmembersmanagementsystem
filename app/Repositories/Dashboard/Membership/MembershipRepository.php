<?php

namespace App\Repositories\Dashboard\Membership;


use App\Models\Dashboard\Membership;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class MembershipRepository implements IMembershipRepositoryContract
{
    protected $membership;

    public function __construct(Membership $membership)
    {
        $this->membership = $membership;
    }

    public function create($request)
    {
        $input = $request->all();
        $input['renewal_date'] = $this->setMembershipExpiryDate($input['membership_year']);

        $this->membership->create($input);
    }

    private function setMembershipExpiryDate($membership_year)
    {
        $membership_year ++;
        return toEnglishDate($membership_year.'-'.'01-01');
    }
}