<?php

namespace App\Repositories\Dashboard\Member;

use App\Exceptions\GeneralException;
use App\Models\Dashboard\Member;
use App\Repositories\Dashboard\Membership\IMembershipRepositoryContract;
use App\Repositories\Dashboard\Payments\IPaymentRepositoryContract;
use Illuminate\Http\Request;

/**
 * Class EloquentMemberRepository
 * @property IMembershipRepositoryContract membership
 * @property IPaymentRepositoryContract payment
 * @package App\Repositories\Member
 */
class MemberRepository implements IMemberRepositoryContract
{
    private $member;

    public function __construct(
        Member $member,
        IMembershipRepositoryContract $membership,
        IPaymentRepositoryContract $payment
    )
    {
        $this->member = $member;
        $this->membership = $membership;
        $this->payment = $payment;
    }

    /**
     * @param $id
     * @param bool $withMembership
     * @param bool $withPayments
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws GeneralException
     */
    public function findOrThrowException($id, $withMembership = false, $withPayments = false)
    {
        if (! is_null(Member::find($id))) {
            if ($withMembership) {
                return Member::with('membership')
                    ->find($id);
            }
            else if ($withPayments) {
                return Member::with('payments')
                    ->find($id);
            }
            else if ($withMembership && $withPayments) {
                return Member::with('membership')
                    ->with('payments')
                    ->find($id);
            }

            return Member::findorfail($id);
        }

        throw new GeneralException('Members not found');
    }

    /**
     * @param $per_page
     * @param int $status
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getMembersPaginated($per_page, $status =1 , $order_by = 'id', $sort = 'asc')
    {
        return Member::where('id','!=','1')
            ->with('membership')
            ->orderBy($order_by, $sort)
            ->paginate($per_page);
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @param  bool    $withMembership
     * @return mixed
     */
    public function getAllMembers($order_by = 'id', $sort = 'asc', $withMembership = false, $withPayments = false)
    {
        if ($withMembership) {
            return Member::with('membership')
                ->whereIn('membership_type', array('A','G'))
                ->where('disabled_status', 1)
                ->where('membership_status', 1)
                ->orderBy($order_by, $sort)
                ->get();
        }
        else if ($withPayments) {
            return Member::with('payments')
                ->where('id','!=','1')
                ->orderBy($order_by, $sort)
                ->get();
        }
        else if ($withMembership && $withPayments) {
            return Member::with('permissions')
                ->with('payments')
                ->orderBy($order_by, $sort)
                ->get();
        }

        return Member::orderBy($order_by, $sort)
            ->get();
    }

    public function create(Request $input)
    {
        $member_id = $this->createMember($input);

        $member = Member::findorfail($member_id['id']);

        $member->registration_id = $member_id['id'];

        $this->resetValues($member);

        $member->save();

        $input['member_id'] = $member->id;
        $input['purpose'] = 'Membership Fee';

        $this->membership->create($input);

        $this->payment->create($input, $member);
    }

    /**
     * Reset values of Current Address depending on the chosen country
     * @param Member $member
     */
    public function resetValues(Member $member) {
        // 0 is Nepal
        if ($member->curr_country == '0') {
            $member->state = '';
            $member->zip_or_postal = '';
            $member->city = '';
            $member->street = '';
            $member->house_number = '';
        } else {
            $member->curr_district = '';
            $member->curr_vdc = '';
            $member->curr_ward = '';
        }
    }

    /**
     * @param Request $input
     * @return static
     * @internal param $photo
     */
    protected function createMember(Request $input)
    {
        return $this->member->create($input->all());
    }
}
