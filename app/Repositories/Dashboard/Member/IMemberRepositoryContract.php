<?php

namespace App\Repositories\Dashboard\Member;


use App\Http\Requests\MembersRequest;
use Illuminate\Http\Request;
use App\Models\Dashboard\Member;

interface IMemberRepositoryContract
{
    /**
     * @param $id
     * @param bool $withMembership
     * @param bool $withPayments
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws GeneralException
     */
    public function findOrThrowException($id, $withMembership = false, $withPayments = false);

    /**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getMembersPaginated($per_page, $status,  $order_by = 'id', $sort = 'asc');

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @param  bool    $withMembership
     * @return mixed
     */
    public function getAllMembers($order_by = 'id', $sort = 'asc', $withMembership = false, $withPayments = false);


    /**
     * @param Request $request
     * @return mixed
     * @internal param $doc
     */
    public function create(Request $request);

    public function resetValues(Member $member);

}