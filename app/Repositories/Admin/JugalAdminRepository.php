<?php
namespace App\Repositories\Admin;

use App\Exceptions\GeneralException;
use App\Models\Dashboard\JugalAdmin;

class JugalAdminRepository implements IJugalAdminRepositoryContract
{
    /**
     * JugalAdmin Object
     */
    private $jugalAdmin;

    public function __construct(JugalAdmin $jugalAdmin)
    {
        $this->jugalAdmin = $jugalAdmin;
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id)
    {
        if (! is_null(JugalAdmin::find($id))) {
            return JugalAdmin::findorfail($id);
        }

        throw new GeneralException('No Configuration found');
    }

    /**
     * @param  string $order_by
     * @param  string $sort
     * @return mixed
     */
    public function getAllConfigs($order_by = 'id', $sort = 'asc')
    {
        $jugalAdmin= JugalAdmin::orderBy($order_by, $sort)
            ->get();

        foreach ($jugalAdmin as $row) {
            $configs[$row->key] = $row->value;
        }

        return $configs;
    }

    /**
     * @param $input
     * @return mixed
     * @throws GeneralException
     */
    public function createConfigs($input)
    {
        if($this->jugalAdmin->create($input))
        {
            return true;
        }

        throw new GeneralException(trans('exceptions.dashboard.alliances.create_error'));
    }
}