<?php

use Illuminate\Database\Seeder;

class PermissionDependenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table(config('acl.permission_dependencies_table'))->truncate();
        } elseif (env('DB_CONNECTION') == 'sqlite') {
            DB::statement('DELETE FROM ' . config('acl.permission_dependencies_table'));
        } else {
            //For PostgreSQL or anything else
            DB::statement('TRUNCATE TABLE ' . config('acl.permission_dependencies_table'));
        }

        $permission1 = DB::table(config('acl.permission_table'))
            ->where('slug', 'view-backend')->first()->id;
        $permission2 = DB::table(config('acl.permission_table'))
            ->where('slug', 'view-access-management')->first()->id;
        $permission3 = DB::table(config('acl.permission_table'))
            ->where('slug', 'manage-members')->first()->id;
//        $permission4 = DB::table(config('acl.permission_table'))
//            ->where('slug', 'manage-payments')->first()->id;

        /**
         * View access management needs view backend
         */
        DB::table(config('acl.permission_dependencies_table'))->insert([
            'permission_id' => $permission2,
            'dependency_id' => $permission1
        ]);

        /**
         * All of the access permissions need view access management and view backend
         * Starts at id = 3 to skip view-backend, view-access-management
         */
        $backendDependency = DB::table(config('acl.permission_table'))->where('id', '>', 2)->pluck('id');

        foreach ($backendDependency as $remainingPermissionId) {
            DB::table(config('acl.permission_dependencies_table'))->insert([
                'permission_id' => $remainingPermissionId,
                'dependency_id' => $permission1,
            ]);
        }

        $aclManagement = DB::table(config('acl.permission_table'))
            ->where('id', '>', 2)
            ->where('id', '<', 22)
            ->pluck('id');

        foreach ($aclManagement as $remainingPermissionId) {
            DB::table(config('acl.permission_dependencies_table'))->insert([
                'permission_id' => $remainingPermissionId,
                'dependency_id' => $permission2,
            ]);
        }

        $membersManagement = DB::table(config('acl.permission_table'))
            ->where('id', '>', 22)
            ->where('id', '<', 27)
            ->pluck('id');
        foreach ($membersManagement as $remainingPermissionId) {
            DB::table(config('acl.permission_dependencies_table'))->insert([
                'permission_id' => $remainingPermissionId,
                'dependency_id' => $permission3,
            ]);
        }

//        $paymentManagement = DB::table(config('acl.permission_table'))
//            ->where('id', '>', 27)
//            ->where('id', '<', 30)
//            ->pluck('id');
//        foreach ($paymentManagement as $remainingPermissionId) {
//            DB::table(config('acl.permission_dependencies_table'))->insert([
//                'permission_id' => $remainingPermissionId,
//                'dependency_id' => $permission4,
//            ]);
//        }

        /**
         * Other dependencies here, follow above structure
         * If you have many it would be a good idea to break this up into different files and require them here
         */

        /**
         * End other dependencies
         */

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
