<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table('roles')->truncate();
        }

        //Add the master administrator, user id of 1
        $roles = [
            [
                'name'              => 'Super Admin',
                'slug'              => 'super-admin-role',
                'description'       => 'Administrator role of the site',
                'all'               => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Accountant',
                'slug'              => 'accountant-role',
                'description'       => 'Accountant role of the site',
                'all'               => 0,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Human Resource',
                'slug'              => 'hr-role',
                'description'       => 'HR role of the site',
                'all'               => 0,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Users',
                'slug'              => 'normal-role',
                'description'       => 'Normal role of the site to view members',
                'all'               => 0,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ];

        DB::table('roles')->insert($roles);

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
