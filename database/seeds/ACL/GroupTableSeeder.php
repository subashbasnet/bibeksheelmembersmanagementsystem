<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupTableSeeder extends Seeder
{
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table(config('access.permission_group_table'))->truncate();
        } elseif (env('DB_CONNECTION') == 'sqlite') {
            DB::statement('DELETE FROM ' . config('acl.group_table'));
        } else {
            //For PostgreSQL or anything else
            DB::statement('TRUNCATE TABLE ' . config('acl.group_table'));
        }

        /**
         * Create the Access groups
         */
        $group_model        = config('acl.group_model');

        $access             = new $group_model;
        $access->name       = 'Access Control List';
        $access->sort       = 1;
        $access->save();

        $user             = new $group_model;
        $user->name       = 'User';
        $user->sort       = 1;
        $user->parent_id  = $access->id;
        $user->save();

        $role             = new $group_model;
        $role->name       = 'Role';
        $role->sort       = 2;
        $role->parent_id  = $access->id;
        $role->save();

        $permission             = new $group_model;
        $permission->name       = 'Permission';
        $permission->sort       = 3;
        $permission->parent_id  = $access->id;
        $permission->save();

        $members          =        new $group_model;
        $members->name      =       'Members Management';
        $members->sort      =       1;
        $members->save();

        $members          =        new $group_model;
        $members->name      =       'Payment Management';
        $members->sort      =       1;
        $members->save();

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
