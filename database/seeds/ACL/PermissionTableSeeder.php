<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table(config('acl.permission_table'))->truncate();
        }

        $permission_model          = config('acl.permission_model');

        /**
         * Misc Access Permissions
         */
        $viewBackend               = new $permission_model;
        $viewBackend->slug         = 'view-backend';
        $viewBackend->name         = 'View Backend';
        $viewBackend->built_in     = true;
        $viewBackend->group_id     = 1;
        $viewBackend->sort         = 1;
        $viewBackend->save();

        $viewAccessManagement               = new $permission_model;
        $viewAccessManagement->slug         = 'view-access-management';
        $viewAccessManagement->name = 'View Access Management';
        $viewAccessManagement->built_in       = true;
        $viewAccessManagement->group_id     = 1;
        $viewAccessManagement->sort         = 2;
        $viewAccessManagement->save();

        /**
         * ACL Permissions - Users
         */
        $createUsers               = new $permission_model;
        $createUsers->slug         = 'create-users';
        $createUsers->name = 'Create Users';
        $createUsers->built_in       = true;
        $createUsers->group_id     = 2;
        $createUsers->sort         = 5;
        $createUsers->save();

        $editUsers               = new $permission_model;
        $editUsers->slug         = 'edit-users';
        $editUsers->name = 'Edit Users';
        $editUsers->built_in       = true;
        $editUsers->group_id     = 2;
        $editUsers->sort         = 6;
        $editUsers->save();

        $deleteUsers               = new $permission_model;
        $deleteUsers->slug         = 'delete-users';
        $deleteUsers->name = 'Delete Users';
        $deleteUsers->built_in       = true;
        $deleteUsers->group_id     = 2;
        $deleteUsers->sort         = 7;
        $deleteUsers->save();

        $changeUserPassword               = new $permission_model;
        $changeUserPassword->slug         = 'change-user-password';
        $changeUserPassword->name = 'Change User Password';
        $changeUserPassword->built_in       = true;
        $changeUserPassword->group_id     = 2;
        $changeUserPassword->sort         = 8;
        $changeUserPassword->save();

        $deactivateUser               = new $permission_model;
        $deactivateUser->slug         = 'deactivate-users';
        $deactivateUser->name = 'Deactivate Users';
        $deactivateUser->built_in       = true;
        $deactivateUser->group_id     = 2;
        $deactivateUser->sort         = 9;
        $deactivateUser->save();

        $reactivateUser               = new $permission_model;
        $reactivateUser->slug         = 'reactivate-users';
        $reactivateUser->name = 'Re-Activate Users';
        $reactivateUser->built_in       = true;
        $reactivateUser->group_id     = 2;
        $reactivateUser->sort         = 11;
        $reactivateUser->save();

        $restoreUser               = new $permission_model;
        $restoreUser->slug         = 'restore-users';
        $restoreUser->name = 'Restore Users';
        $restoreUser->built_in       = true;
        $restoreUser->group_id     = 2;
        $restoreUser->sort         = 13;
        $restoreUser->save();

        $permanentlyDeleteUser               = new $permission_model;
        $permanentlyDeleteUser->slug         = 'permanently-delete-users';
        $permanentlyDeleteUser->name = 'Permanently Delete Users';
        $permanentlyDeleteUser->built_in       = true;
        $permanentlyDeleteUser->group_id     = 2;
        $permanentlyDeleteUser->sort         = 14;
        $permanentlyDeleteUser->save();

        $resendConfirmationEmail               = new $permission_model;
        $resendConfirmationEmail->slug         = 'resend-user-confirmation-email';
        $resendConfirmationEmail->name = 'Resend Confirmation E-mail';
        $resendConfirmationEmail->built_in       = true;
        $resendConfirmationEmail->group_id     = 2;
        $resendConfirmationEmail->sort         = 15;
        $resendConfirmationEmail->save();

        /**
         * Role
         */
        $createRoles               = new $permission_model;
        $createRoles->slug         = 'create-roles';
        $createRoles->name = 'Create Roles';
        $createRoles->built_in       = true;
        $createRoles->group_id     = 3;
        $createRoles->sort         = 2;
        $createRoles->save();

        $editRoles               = new $permission_model;
        $editRoles->slug         = 'edit-roles';
        $editRoles->name = 'Edit Roles';
        $editRoles->built_in       = true;
        $editRoles->group_id     = 3;
        $editRoles->sort         = 3;
        $editRoles->save();

        $deleteRoles               = new $permission_model;
        $deleteRoles->slug         = 'delete-roles';
        $deleteRoles->name = 'Delete Roles';
        $deleteRoles->built_in       = true;
        $deleteRoles->group_id     = 3;
        $deleteRoles->sort         = 4;
        $deleteRoles->save();

        /**
         * Permission Group
         */
        $createPermissionGroups               = new $permission_model;
        $createPermissionGroups->slug         = 'create-permission-groups';
        $createPermissionGroups->name = 'Create Permission Groups';
        $createPermissionGroups->built_in       = true;
        $createPermissionGroups->group_id     = 4;
        $createPermissionGroups->sort         = 1;
        $createPermissionGroups->save();

        $editPermissionGroups               = new $permission_model;
        $editPermissionGroups->slug         = 'edit-permission-groups';
        $editPermissionGroups->name = 'Edit Permission Groups';
        $editPermissionGroups->built_in       = true;
        $editPermissionGroups->group_id     = 4;
        $editPermissionGroups->sort         = 2;
        $editPermissionGroups->save();

        $deletePermissionGroups               = new $permission_model;
        $deletePermissionGroups->slug         = 'delete-permission-groups';
        $deletePermissionGroups->name = 'Delete Permission Groups';
        $deletePermissionGroups->built_in       = true;
        $deletePermissionGroups->group_id     = 4;
        $deletePermissionGroups->sort         = 3;
        $deletePermissionGroups->save();

        $sortPermissionGroups               = new $permission_model;
        $sortPermissionGroups->slug         = 'sort-permission-groups';
        $sortPermissionGroups->name = 'Sort Permission Groups';
        $sortPermissionGroups->built_in       = true;
        $sortPermissionGroups->group_id     = 4;
        $sortPermissionGroups->sort         = 4;
        $sortPermissionGroups->save();

        /**
         * ACL Permissions - Permission Management
         */
        $createPermissions               = new $permission_model;
        $createPermissions->slug         = 'create-permissions';
        $createPermissions->name = 'Create Permissions';
        $createPermissions->built_in       = true;
        $createPermissions->group_id     = 4;
        $createPermissions->sort         = 5;
        $createPermissions->save();

        $editPermissions               = new $permission_model;
        $editPermissions->slug         = 'edit-permissions';
        $editPermissions->name = 'Edit Permissions';
        $editPermissions->built_in       = true;
        $editPermissions->group_id     = 4;
        $editPermissions->sort         = 6;
        $editPermissions->save();

        $deletePermissions               = new $permission_model;
        $deletePermissions->slug         = 'delete-permissions';
        $deletePermissions->name = 'Delete Permissions';
        $deletePermissions->built_in       = true;
        $deletePermissions->group_id     = 4;
        $deletePermissions->sort         = 7;
        $deletePermissions->save();

        //Members Management Permissions
        $manageMembers               = new $permission_model;
        $manageMembers->slug         = 'manage-members';
        $manageMembers->name = 		'Manage Members';
        $manageMembers->built_in       = true;
        $manageMembers->group_id     = 5;
        $manageMembers->sort         = 5;
        $manageMembers->save();

        $viewMembers               = new $permission_model;
        $viewMembers->slug         = 'view-members';
        $viewMembers->name = 		'View Members';
        $viewMembers->built_in       = true;
        $viewMembers->group_id     = 5;
        $viewMembers->sort         = 5;
        $viewMembers->save();

        $createMembers               = new $permission_model;
        $createMembers->slug         = 'create-members';
        $createMembers->name = 'Create Members';
        $createMembers->built_in       = true;
        $createMembers->group_id     = 5;
        $createMembers->sort         = 5;
        $createMembers->save();

        $editMembers               = new $permission_model;
        $editMembers->slug         = 'edit-members';
        $editMembers->name = 'Edit Members';
        $editMembers->built_in       = true;
        $editMembers->group_id     = 5;
        $editMembers->sort         = 6;
        $editMembers->save();

        $deleteMembers               = new $permission_model;
        $deleteMembers->slug         = 'delete-members';
        $deleteMembers->name = 'Delete Members';
        $deleteMembers->built_in       = true;
        $deleteMembers->group_id     = 5;
        $deleteMembers->sort         = 7;
        $deleteMembers->save();

        //Payment Management permissions
        $viewPayments               = new $permission_model;
        $viewPayments->slug         = 'view-payments';
        $viewPayments->name = 		'View Payments';
        $viewPayments->built_in       = true;
        $viewPayments->group_id     = 6;
        $viewPayments->sort         = 5;
        $viewPayments->save();

        $createPayments               = new $permission_model;
        $createPayments->slug         = 'create-payments';
        $createPayments->name         = 'Create Payments';
        $createPayments->built_in       = true;
        $createPayments->group_id     = 6;
        $createPayments->sort         = 5;
        $createPayments->save();

        $editPayments               = new $permission_model;
        $editPayments->slug         = 'edit-payments';
        $editPayments->name         = 'Edit Payments';
        $editPayments->built_in     = true;
        $editPayments->group_id     = 6;
        $editPayments->sort         = 6;
        $editPayments->save();

        $deletePayments               = new $permission_model;
        $deletePayments->slug         = 'delete-payments';
        $deletePayments->name         = 'Delete Payments';
        $deletePayments->built_in       = true;
        $deletePayments->group_id     = 6;
        $deletePayments->sort         = 7;
        $deletePayments->save();

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
