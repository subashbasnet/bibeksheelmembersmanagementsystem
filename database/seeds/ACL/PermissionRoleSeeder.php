<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table('permission_role')->truncate();
        }

        //Add the master administrator, user id of 1
        $permission_role = [
            [
                'permission_id'         => 1,
                'role_id'               => 1,
            ],
            [
                'permission_id'         => 1,
                'role_id'               => 2,
            ],
            [
                'permission_id'         => 1,
                'role_id'               => 3,
            ],
            [
                'permission_id'         => 1,
                'role_id'               => 4,
            ],
            [
                'permission_id'         => 22,
                'role_id'               => 3,
            ],
            [
                'permission_id'         => 23,
                'role_id'               => 3,
            ],
            [
                'permission_id'         => 24,
                'role_id'               => 3,
            ],
            [
                'permission_id'         => 25,
                'role_id'               => 3,
            ],
            [
                'permission_id'         => 26,
                'role_id'               => 3,
            ],
            [
                'permission_id'         => 22,
                'role_id'               => 2,
            ],
            [
                'permission_id'         => 23,
                'role_id'               => 2,
            ],
            [
                'permission_id'         => 27,
                'role_id'               => 2,
            ],
            [
                'permission_id'         => 28,
                'role_id'               => 2,
            ],
            [
                'permission_id'         => 29,
                'role_id'               => 2,
            ],
        ];

        DB::table('permission_role')->insert($permission_role);

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
