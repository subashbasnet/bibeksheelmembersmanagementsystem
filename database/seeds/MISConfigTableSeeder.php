<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MISConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table('mis_config')->truncate();
        }

        $config = [
            [
                'key'   =>  'sitename',
                'value' =>  'Bibeksheel InfoSys'
            ],
            [
                'key'   =>  'sitename_short',
                'value' =>  'BNInfoSys'
            ],
            [
                'key'   =>  'site_description',
                'value' =>  'This is Information System of Bibeksheel Nepali.'
            ],
            [
                'key'   =>  'sidebar_search',
                'value' =>  '1'
            ],
            [
                'key'   =>  'show_messages',
                'value' =>  '1'
            ],
            [
                'key'   =>  'show_notifications',
                'value' =>  '1'
            ],
            [
                'key'   =>  'show_tasks',
                'value' =>  '1'
            ],
            [
                'key'   =>  'show_rightsidebar',
                'value' =>  '1'
            ],
            [
                'key'   =>  'skin',
                'value' =>  'skin-blue'
            ],
            [
                'key'   =>  'layout',
                'value' =>  'fixed'
            ],
            [
                'key'   =>  'admin_email',
                'value' =>  'rajanad.bibeksheel@gmail.com'
            ],
            [
                'key'   =>  'website',
                'value' =>  'www.leadnepal.com'
            ],
        ];

        DB::table('mis_config')->insert($config);

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}