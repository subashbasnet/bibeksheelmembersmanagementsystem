<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender')->length(100);
            $table->string('to')->length(100);
            $table->string('cc')->length(100);
            $table->string('bcc')->length(100);
            $table->string('reply_to');
            $table->string('subject');
            $table->string('priority');
            $table->string('body')->length(1000);
            $table->string('attachment');
            $table->dateTime('schedule_date');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mailboxes');
    }
}
