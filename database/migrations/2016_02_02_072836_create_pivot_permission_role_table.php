<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotPermissionRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('acl.permission_role_pivot_table'), function (Blueprint $table) {
            $table->unsignedInteger('permission_id');
            $table->foreign('permission_id')
                ->references('id')
                ->on(config('acl.permission_table'))
                ->onDelete('Cascade')
                ->onUpdate('Cascade');

            $table->unsignedInteger('role_id');
            $table->foreign('role_id')
                ->references('id')
                ->on(config('acl.role_table'))
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(config('acl.permission_role_pivot_table'));
    }
}
