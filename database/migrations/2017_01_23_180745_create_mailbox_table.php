<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailbox', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender')->length(100);
            $table->string('to')->length(100);
            $table->string('cc')->length(100);
            $table->string('bcc')->length(100);
            $table->string('reply_to');
            $table->string('subject');
            $table->string('priority');
            $table->string('body')->length(1000);
            $table->string('path_to_file');
            $table->dateTime('schedule_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mailbox');
    }
}
