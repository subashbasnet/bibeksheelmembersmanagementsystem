<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlliancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alliances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('post', 100);
            $table->string('organization',256);
            $table->string('address',256);
            $table->string('email',256);
            $table->integer('mobile')->length(20)->unique();
            $table->integer('phone_1')->length(20);
            $table->integer('phone_2')->length(20);
            $table->string('reference',100);
            $table->string('website',200);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alliances');
    }
}
