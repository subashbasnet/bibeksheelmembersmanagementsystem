<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('acl.user_role_pivot_table'), function (Blueprint $table) {
            $table->unsignedInteger('role_id');
            $table->foreign('role_id')
                ->references('id')
                ->on(config('acl.role_table'))
                ->onDelete('Cascade')
                ->onUpdate('Cascade');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('Cascade')
                ->onUpdate('Cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(config('acl.user_role_pivot_table'));
    }
}
