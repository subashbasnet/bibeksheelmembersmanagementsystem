<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value determines the name of your application.
    |
    */

    'app_name' => 'Bibeksheel Nepali MMS',

    /*
    |--------------------------------------------------------------------------
    | Application version
    |--------------------------------------------------------------------------
    |
    | This value determines the version of your application.
    |
    */

    'app_version' => '0.1',

    /*
    |--------------------------------------------------------------------------
    | Organization Name
    |--------------------------------------------------------------------------
    |
    | This value determines the name of your organization.
    |
    */

    'org_name' => 'Bibeksheel Nepali Party',

    /*
    |--------------------------------------------------------------------------
    | Organization Website
    |--------------------------------------------------------------------------
    |
    | This value determines the official website of your organization
    |
    */

    'org_website' => 'http://www.leadnepal.org',

    /**
     |-------------------------------------------------------------------------
     | Members Attributes
     |-------------------------------------------------------------------------
     *
     *  This value determines default parameter for Members
     */

    'member_initial_payment' => [

        'membership_fee' => 1000,

        'purpose'       => 'Membership Fee',

        'default_payment_modes' => 'cash'
    ],

];