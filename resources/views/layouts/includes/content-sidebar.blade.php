<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="{{ asset('assets/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p>{{ get_users_name() }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
    </div>
</form>
<!-- search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
        <a href="{{ url('home') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>

    {{--If user has permission to manage users then only see this menu--}}
    @can('manage_users')
        <li class="treeview">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Users</span>
                <span class="label label-primary pull-right"></span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url('users') }}"><i class="fa fa-user-md"></i> View Users</a></li>
                <li><a href="{{ url('users/create') }}"><i class="fa fa-user-plus"></i> Add Users</a></li>
                <li><a href="#"><i class="fa fa-user-plus"></i> Deleted Users</a></li>
            </ul>
        </li>
    @endcan

    {{--If users have permission to manage members then only see this menu--}}
    @can('manage_members')
        <li class="treeview">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Members</span>
                <span class="label label-primary pull-right"></span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url('members') }}"><i class="fa fa-user-md"></i> View Members</a></li>
                <li><a href="{{ url('members/create') }}"><i class="fa fa-user-plus"></i> Add Members</a></li>
                <li><a href="#"><i class="fa fa-user-plus"></i> Deleted Members</a></li>
            </ul>
        </li>
    @endcan
    <li class="treeview">
        <a href="#">
            <i class="fa fa-calendar"></i>
            <span>Events</span>
            <span class="label label-primary pull-right">4</span>
        </a>
        <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-calendar-o"></i> All Events</a></li>
            <li><a href="#"><i class="fa fa-calendar-plus-o"></i> Add Event</a></li>
        </ul>
    </li>
</ul>