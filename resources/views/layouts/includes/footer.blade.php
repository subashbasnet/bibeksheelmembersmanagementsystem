<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> {{ show_app_version() }}
    </div>
    <strong>Copyright &copy; <a href="{{ show_organization_website() }}">{{ show_organization_name() }}</a>.</strong> All rights
    reserved.
</footer>