<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>@yield('title', app_name())</title>

@yield('meta')

<!-- Styles -->
    @yield('before-styles-end')

    <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/ionicons.min.css') }}">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('assets/css/skins/'.\App\Models\Dashboard\JugalAdmin::getByKey('skin').'.css') }}" rel="stylesheet" type="text/css" />
    {{--<link rel="stylesheet" href="{{ asset('assets/css/_all-skins.css') }}">--}}
    {{--<link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet">--}}
@yield('after-styles-end')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="{{ \App\Models\Dashboard\JugalAdmin::getByKey('skin') }} {{ \App\Models\Dashboard\JugalAdmin::getByKey('layout') }} @if(\App\Models\Dashboard\JugalAdmin::getByKey('layout') == 'sidebar-mini') sidebar-collapse @endif">
<div class="wrapper">
@include('dashboard.includes.header')

@include('dashboard.includes.sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('page-header')
        </section>

        <!-- Main content -->
        <section class="content">
            @include('includes.partials.messages')
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('dashboard.includes.footer')
</div><!-- ./wrapper -->

<!-- JavaScripts -->
@yield('before-scripts-end')
<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
@yield('after-scripts-end')
</body>
</html>