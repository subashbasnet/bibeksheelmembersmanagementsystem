@extends ('dashboard.layouts.master')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.deleted'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.acl.users.header') }}
        <small>{{ trans('labels.admin.acl.users.deleted') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.admin.acl.users.deleted') }}</h3>

        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.admin.acl.users.table.id') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.name') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.email') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.confirmed') }}</th>
                        <th>{{ trans('labels.admin.acl.users.table.roles') }}</th>
                        <th class="visible-lg">{{ trans('labels.admin.acl.users.table.created') }}</th>
                        <th class="visible-lg">{{ trans('labels.admin.acl.users.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($users->count())
                        @foreach ($users as $user)
                            <tr>
                                <td>{!! $user->id !!}</td>
                                <td>{!! $user->full_name !!}</td>
                                <td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
                                <td>{!! $user->confirmed_label !!}</td>
                                <td>
                                    @if ($user->roles()->count() > 0)
                                        @foreach ($user->roles as $role)
                                            {!! $role->name !!}<br/>
                                        @endforeach
                                    @else
                                        None
                                    @endif
                                </td>
                                <td class="visible-lg">{!! $user->created_at->diffForHumans() !!}</td>
                                <td class="visible-lg">{!! $user->updated_at->diffForHumans() !!}</td>
                                <td>
                                    @permission('undelete-users')
                                        <a href="{{route('dashboard.user.restore', $user->id)}}" class="btn btn-xs btn-success" name="restore_user"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Restore User"></i></a>
                                    @end

                                    @permission('permanently-delete-users')
                                        <a href="{{route('dashboard.user.delete-permanently', $user->id)}}" class="btn btn-xs btn-danger" name="delete_user_perm"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete Permanently"></i></a>
                                    @end
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="9">{{ trans('labels.admin.acl.users.no_deleted_users') }}</td>
                    @endif
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {!! $users->total() !!} {{ trans_choice('labels.admin.acl.users.table.total', $users->total()) }}
            </div>

            <div class="pull-right">
                {!! $users->render() !!}
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@section('after-scripts-end')
	<script>
		$(function() {
            @permission('permanently-delete-users')
                $("a[name='delete_user_perm']").click(function(e) {
                    e.preventDefault();

                    swal({
                        title: "Are you sure?",
                        text: "Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false
                    }, function(isConfirmed){
                        if (isConfirmed){
                            window.location = $("a[name='delete_user_perm']").attr('href');
                        }
                    });
                });
            @end

            @permission('reactivate-users')
                $("a[name='restore_user']").click(function(e) {
                    e.preventDefault();

                    swal({
                        title: "Are you Sure?",
                        text: "Restore this user to its original state?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false
                    }, function(isConfirmed){
                        if (isConfirmed){
                            window.location = $("a[name='restore_user']").attr('href');
                        }
                    });
                });
            @end
		});
	</script>
@stop
