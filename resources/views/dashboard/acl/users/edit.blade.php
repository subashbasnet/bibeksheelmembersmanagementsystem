@extends ('dashboard.layouts.master')

@section ('title', trans('labels.admin.acl.users.header') . ' | ' . trans('labels.admin.acl.users.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.acl.users.header') }}
        <small>{{ trans('labels.admin.acl.users.edit') }}</small>
    </h1>
@endsection

@section('content')
    {!! Form::model($user, ['route' => ['dashboard.users.update', $user->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.admin.acl.users.edit') }}</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('full_name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('full_name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('email', 'E-mail', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                    </div>
                </div><!--form control-->

                @if ($user->id != 1)
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Active</label>
                        <div class="col-lg-1">
                            <input type="checkbox" value="1" name="status" {{$user->status == 1 ? 'checked' : ''}} />
                        </div>
                    </div><!--form control-->

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Confirmed</label>
                        <div class="col-lg-1">
                            <input type="checkbox" value="1" name="confirmed" {{$user->confirmed == 1 ? 'checked' : ''}} disabled/>
                        </div>
                    </div><!--form control-->

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Associated Roles</label>
                        <div class="col-lg-3">
                            @if (count($roles) > 0)
                                @foreach($roles as $role)
                                    <input type="checkbox" value="{{$role->id}}" name="assignees_roles[]" {{in_array($role->id, $user_roles) ? 'checked' : ''}} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{!! $role->name !!}</label>
                                        <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                            (
                                                <span class="show-text">Show</span>
                                                <span class="hide-text hidden">Hide</span>
                                                Permissions
                                            )
                                        </a>
                                    <br/>
                                    <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                        @if ($role->all)
                                            All Permissions<br/><br/>
                                        @else
                                            @if (count($role->permissions) > 0)
                                                <blockquote class="small">{{--
                                            --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                    @endforeach
                                                </blockquote>
                                            @else
                                                No Permissions<br/><br/>
                                            @endif
                                        @endif
                                    </div><!--permission list-->
                                @endforeach
                            @else
                                No Roles to set
                            @endif
                        </div>
                    </div><!--form control-->
                @endif
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    <a href="{{route('dashboard.users.index')}}" class="btn btn-danger btn-xs">Cancel</a>
                </div>

                <div class="pull-right">
                    <input type="submit" class="btn btn-success btn-xs" value="Update" />
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

        @if ($user->id == 1)
            {!! Form::hidden('status', 1) !!}
            {!! Form::hidden('confirmed', 1) !!}
            {!! Form::hidden('assignees_roles[]', 1) !!}
        @endif

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    <script>
        $(function() {
            $(".show-permissions").click(function(e) {
                e.preventDefault();
                var $this = $(this);
                var role = $this.data('role');
                var permissions = $(".permission-list[data-role='"+role+"']");
                var hideText = $this.find('.hide-text');
                var showText = $this.find('.show-text');
                // console.log(permissions); // for debugging

                // show permission list
                permissions.toggleClass('hidden');

                // toggle the text Show/Hide for the link
                hideText.toggleClass('hidden');
                showText.toggleClass('hidden');
            });
        });

        $(function() {
            //Checks checkboxes for dependencies
            $("input[name='permission_user[]']").change(function() {
                checkDependencies($(this));
            });

            //Recursively check dependencies
            function checkDependencies(item) {
                var dependencies = item.data('dependencies');
                var count = 0;

                if (dependencies.length) {
                    for (var i = 0; i < dependencies.length; i++) {
                        if (item.is(":checked")) {
                            var permission = $("#permission-" + dependencies[i]);

                            if (! permission.is(":checked"))
                                permission.prop("checked", true);

                            count++;

                            if (count == 1)
                                checkDependencies(permission);
                        }
                    }
                }
            }
        });
    </script>
@stop
