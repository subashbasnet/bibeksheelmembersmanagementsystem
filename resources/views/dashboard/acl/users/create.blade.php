@extends ('dashboard.layouts.master')

@section ('title', trans('labels.admin.acl.users.header') . ' | ' . trans('labels.admin.acl.users.create'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.acl.users.header') }}
        <small>{{ trans('labels.admin.acl.users.create') }}</small>
    </h1>
@endsection

@section('content')
    {!! Form::open(['route' => 'dashboard.users.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.admin.acl.users.create') }}</h3>
        </div>

            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('password', 'Password', ['class' => 'col-lg-2 control-label', 'placeholder' => 'Password']) !!}
                    <div class="col-lg-10">
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-lg-2 control-label', 'placeholder' => 'Confirm Password']) !!}
                    <div class="col-lg-10">
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">Confirmed</label>
                    <div class="col-lg-1">
                        <input type="checkbox" value="1" name="confirmed" checked="checked" />
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">Send Confirmation Email<br/>
                        <small>(If confirmed is off)</small>
                    </label>
                    <div class="col-lg-1">
                        <input type="checkbox" value="1" name="confirmation_email" />
                    </div>
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">Associated Roles</label>
                    <div class="col-lg-4">
                        @if (count($roles) > 0)
                            @foreach($roles as $role)
                                <input type="checkbox" value="{{$role->id}}" name="assignees_roles[]" id="role-{{$role->id}}" />
                                <label for="role-{{$role->id}}">{!! $role->name !!}</label>
                                <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                    (
                                        <span class="show-text">Show</span>
                                        <span class="hide-text hidden">Hide</span>
                                        Permissions
                                    )
                                </a>
                                <br/>
                                <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                    @if ($role->all)
                                        All Permissions<br/><br/>
                                    @else
                                        @if (count($role->permissions) > 0)
                                            <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                @endforeach
                                            </blockquote>
                                        @else
                                            No Permissions<br/><br/>
                                        @endif
                                    @endif
                                </div><!--permission list-->
                            @endforeach
                        @else
                            No Roles
                        @endif
                    </div>
                </div><!--form control-->
                <div class="pull-right">
                    <input type="submit" class="btn btn-success" value="Create" />
                    <a href="{{route('dashboard.users.index')}}" class="btn btn-danger">Cancel</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('assets/js/acl/permission/script.js') !!}
    {!! Html::script('assets/js/acl/user/script.js') !!}
@stop
