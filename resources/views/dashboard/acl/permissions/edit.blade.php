@extends ('dashboard.layouts.master')

@section ('title', trans('labels.dashboard.acl.permissions.header') . ' | ' . trans('labels.dashboard.acl.permissions.edit'))

@section('page-header')
    <h1>
        Permissions Management
        <small>Edit Permissions</small>
    </h1>
@endsection

@section('content')
    {!! Form::model($permission, ['route' => ['dashboard.permissions.update', $permission->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Permissions</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#general" aria-controls="general" role="tab" data-toggle="tab">
                                General
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#dependencies" aria-controls="dependencies" role="tab" data-toggle="tab">
                                Dependencies
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="general" style="padding-top:20px">

                            <div class="form-group">
                                {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                {!! Form::label('slug', 'Slug', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'slug']) !!}
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                {!! Form::label('group', 'Group', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    <select name="group" class="form-control">
                                        <option value="">None</option>

                                        @foreach ($groups as $group)
                                            <option value="{!! $group->id !!}" {!! $permission->group_id == $group->id ? 'selected' : '' !!}>{!! $group->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                {!! Form::label('sort', 'Sort', ['class' => 'col-lg-2 control-label']) !!}
                                <div class="col-lg-10">
                                    {!! Form::text('sort', null, ['class' => 'form-control', 'placeholder' => 'Sort']) !!}
                                </div>
                            </div><!--form control-->

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Associated Roles</label>
                                <div class="col-lg-3">
                                    @if (count($roles) > 0)
                                        @foreach($roles as $role)
                                            <input type="checkbox" {{$role->id == 1 ? 'disabled' : ''}} {{in_array($role->id, $permission_roles) || ($role->id == 1) ? 'checked' : ""}} value="{{$role->id}}" name="permission_roles[]" id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{!! $role->name !!}</label><br/>
                                            <div class="clearfix"></div>
                                        @endforeach
                                    @else
                                        No Roles to Set
                                    @endif
                                </div>
                            </div><!--form control-->

                        </div><!--general-->

                        <div role="tabpanel" class="tab-pane" id="dependencies" style="padding-top:20px">

                            <div class="alert alert-info">
                                <i class="fa fa-info-circle"></i>
                                {{--{!! getLanguageBlock('backend.lang.access.roles.permissions.dependencies-explanation') !!}--}}
                            </div><!--alert-->

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Dependencies</label>
                                <div class="col-lg-10">
                                    @if (count($permissions))
                                        @foreach (array_chunk($permissions->toArray(), 10) as $perm)
                                            <div class="col-lg-3">
                                                <ul style="margin:0;padding:0;list-style:none;">
                                                    @foreach ($perm as $p)
                                                        <?php
                                                        //Since we are using array format to nicely display the permissions in rows
                                                        //we will just manually create an array of dependencies since we do not have
                                                        //access to the relationship to use the lists() function of eloquent
                                                        //but the relationships are eager loaded in array format now
                                                        $dependencies = [];
                                                        $dependency_list = [];
                                                        if (count($p['dependencies'])) {
                                                            foreach ($p['dependencies'] as $dependency) {
                                                                array_push($dependencies, $dependency['dependency_id']);
                                                                array_push($dependency_list, $dependency['permission']['slug']);
                                                            }
                                                        }
                                                        $dependencies = json_encode($dependencies);
                                                        $dependency_list = implode(", ", $dependency_list);
                                                        ?>

                                                        @if ($p['id'] != $permission->id)
                                                            <li><input type="checkbox" value="{{$p['id']}}" name="dependencies[]" data-dependencies="{!! $dependencies !!}" id="permission-{{$p['id']}}" {!! in_array($p['id'], $permission_dependencies) ? 'checked' : '' !!} /> <label for="permission-{{$p['id']}}" />

                                                                @if ($p['dependencies'])
                                                                    <a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="<strong>Dependencies:</strong> {!! $dependency_list !!}">{!! $p['slug'] !!} <small><strong>(D)</strong></small></a>
                                                                @else
                                                                    {!! $p['slug'] !!}
                                                                    @endif

                                                                    </label></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endforeach
                                    @else
                                        No Permissions to choose from.
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    <a href="{!! route('dashboard.permissions.index') !!}" class="btn btn-danger btn-xs">Cancel</a>
                </div>

                <div class="pull-right">
                    <input type="submit" class="btn btn-success btn-xs" value="Update" />
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('assets/js/acl/dependency/script.js') !!}
@stop