@extends ('dashboard.layouts.master')

@section ('title', trans('labels.dashboard.acl.permissions.header') . ' | ' . trans('labels.dashboard.acl.permissions.groups.create'))

@section('page-header')
    <h1>
        Permissions Management
        <small>Create Groups</small>
    </h1>
@endsection

@section('content')
    {!! Form::open(['route' => 'dashboard.permission-group.store', 'class' => 'form-horizontal', 'role' => 'form']) !!}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Creaate Groups</h3>

            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                    </div>
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    <a href="{!! route('dashboard.permissions.index') !!}" class="btn btn-danger btn-xs">Cancel</a>
                </div>

                <div class="pull-right">
                    <input type="submit" class="btn btn-success btn-xs" value="Create" />
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {!! Form::close() !!}
@stop