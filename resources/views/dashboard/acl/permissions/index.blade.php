@inject('roles', 'App\Repositories\ACL\Role\RoleRepositoryContract')

@extends ('dashboard.layouts.master')

@section ('title', trans('labels.admin.acl.permissions.header'))

@section('page-header')
    <h1>{{ trans('labels.admin.acl.permissions.header') }}</h1>
@endsection

@section('after-styles-end')
    {!! Html::style('assets/css/jquery.nestable.css') !!}
@stop

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.admin.acl.permissions.header') }}</h3>

        </div>

        <div class="box-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#groups" aria-controls="groups" role="tab" data-toggle="tab">
                            All Groups
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#permissions" data-url-tab-hash="#all-permissions" aria-controls="permissions"
                           role="tab" data-toggle="tab">
                            All Permissions
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="groups" style="padding-top:20px">

                        <div class="row">
                            <div class="col-lg-6">

                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i> This section allows you to organize all
                                    permissions into groups for smooth management. Regardless of the group, the permissions
                                    are still individually assigned to each role.
                                </div><!--alert info-->

                                <div class="dd permission-hierarchy">
                                    <ol class="dd-list">
                                        @foreach ($groups as $group)
                                            <li class="dd-item" data-id="{!! $group->id !!}">
                                                <div class="dd-handle">{!! $group->name !!} <span class="pull-right">{!! $group->permissions->count() !!}
                                                        Permissions</span></div>

                                                @if ($group->children->count())
                                                    <ol class="dd-list">
                                                        @foreach($group->children as $child)
                                                            <li class="dd-item" data-id="{!! $child->id !!}">
                                                                <div class="dd-handle">{!! $child->name !!} <span
                                                                            class="pull-right">{!! $child->permissions->count() !!}
                                                                        Permissions</span></div>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                            </li>
                                            @else
                                            </li>
                                            @endif
                                        @endforeach
                                    </ol>
                                </div><!--master-list-->
                            </div><!--col-lg-4-->

                            <div class="col-lg-6">

                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i> If you performed operations in the hierarchy
                                    section without refreshing this page, you will need to refresh to reflect the
                                    changes here.
                                </div><!--alert info-->

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($groups as $group)
                                            <tr>
                                                <td>
                                                    {!! $group->name !!}

                                                    @if ($group->permissions->count())
                                                        <div style="padding-left:40px;font-size:.8em">
                                                            @foreach ($group->permissions as $permission)
                                                                {!! $permission->name !!}<br/>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>{!! $group->action_buttons !!}</td>
                                            </tr>

                                            @if ($group->children->count())
                                                @foreach ($group->children as $child)
                                                    <tr>
                                                        <td style="padding-left:40px">
                                                            <em>{!! $child->name !!}</em>

                                                            @if ($child->permissions->count())
                                                                <div style="padding-left:40px;font-size:.8em">
                                                                    @foreach ($child->permissions as $permission)
                                                                        {!! $permission->name !!}<br/>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                        </td>
                                                        <td>{!! $child->action_buttons !!}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div><!--col-lg-8-->
                        </div><!--row-->

                    </div><!--groups-->

                    <div role="tabpanel" class="tab-pane" id="permissions" style="padding-top:20px">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Permission</th>
                                    <th>Name</th>
                                    <th>Dependencies</th>
                                    <th>Users</th>
                                    <th>Roles</th>
                                    <th>Group</th>
                                    <th>Sort</th>
                                    <th>System</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($permissions as $permission)
                                    <tr>
                                        <td>{!! $permission->name !!}</td>
                                        <td>{!! $permission->name !!}</td>
                                        <td>
                                            @if (count($permission->dependencies))
                                                @foreach($permission->dependencies as $dependency)
                                                    {!! $dependency->permission->name !!}<br/>
                                                @endforeach
                                            @else
                                                <span class="label label-success">None</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if (count($permission->users))
                                                @foreach($permission->users as $user)
                                                    {!! $user->name !!}<br/>
                                                @endforeach
                                            @else
                                                <span class="label label-danger">None</span>
                                            @endif
                                        </td>
                                        <td>
                                            {!! $roles->findOrThrowException(1)->name !!}<br/>
                                            @if (count($permission->roles))
                                                @foreach($permission->roles as $role)
                                                    {!! $role->name !!}<br/>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            @if ($permission->group)
                                                {!! $permission->group->name !!}
                                            @else
                                                <span class="label label-danger">None</span>
                                            @endif
                                        </td>
                                        <td>{!! $permission->sort !!}</td>
                                        <td>{!! $permission->system_label !!}</td>
                                        <td>{!! $permission->action_buttons !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="pull-left">
                            {{ $permissions->total() }} {{ trans_choice('labels.dashbaord.acl.permissions.table.total', $permissions->total()) }}
                        </div>

                        <div class="pull-right">
                            {{ $permissions->render() }}
                        </div>

                        <div class="clearfix"></div>

                    </div><!--permissions-->
                </div>
            </div><!--permission tabs-->
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@section('after-scripts-end')
    {!! Html::script('assets/js/jquery.nestable.js') !!}
@stop
