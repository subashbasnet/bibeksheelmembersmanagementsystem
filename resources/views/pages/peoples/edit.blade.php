@extends('dashboard.layouts.master')

@section ('title', trans('labels.admin.peoples.header') . ' | ' . trans('labels.admin.peoples.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.peoples.header') }}
        <small>{{ trans('labels.admin.peoples.edit') }}</small>
    </h1>
@endsection
@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::model($people, ['method' => 'PUT', 'route' => ['dashboard.peoples.update',$people->id], 'class' => 'form-horizontal', 'id' => 'peoplesForm' , 'files'=>'true']) !!}
            @include('pages.peoples.partials._persnoalInfo_EditForm')
            <hr/>
            @include('pages.peoples.partials._addressInfo_EditForm')

            <div class="form-group">
                <div class="pull-right col-xs-10 ">
                    <button type="reset" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Reset
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Update
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="box-footer"></div>
    </div>
@endsection

@section('after-scripts-end')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
    {!! Html::script('assets/js/member.js') !!}
@endsection