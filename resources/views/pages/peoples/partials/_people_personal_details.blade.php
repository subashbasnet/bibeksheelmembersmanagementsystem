<table class="table">
    <tbody>
        <tr>
            <td><label>Date of Birth: </label></td>
            <td>{{ $people->date_of_birth }}</td>
            <td><label>Age: </label>{{ ' '.$people->getAge() }}</td>
            @if($people->photo_path)
                <td rowspan="4"><img src="{{ $people->photo_path }}" height="150" width="200" /> </td>
            @else
                <td></td>
            @endif

        </tr>
        <tr>
            <td><label>Gender: </label></td>
            <td>{{ $people->gender }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Permanent Address: </label></td>
            <td>{{ $people->perm_vdc.' '.$people->perm_ward.', '.$people->perm_district }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Current Address: </label></td>
            @if($people->curr_country == '0')
            <td>{{ $people->curr_vdc.' '.$people->curr_ward.', '.$people->curr_district }}</td>
            @else
            <td>{{ $people->city.', '.$people->curr_country }}</td>
            @endif
            <td></td>
        </tr>
        <tr>
            <td><label>Email ID: </label></td>
            <td>{{ $people->email }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Phone no: </label></td>
            <td>{{ $people->contact_no }}</td>
            <td><label>Mobile no: </label></td>
            <td>{{ $people->mobile_no }}</td>
        </tr>
        <tr>
            <td><label>Qualification: </label></td>
            <td>{{ $people->qualification}}</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
