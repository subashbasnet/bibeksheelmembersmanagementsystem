@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.header') }}
        <small>{{ trans('labels.admin.members.disabled') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">

            </div>
            @permission('create-members')
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-primary" href="{{ route('dashboard.members.create') }}">
                    <i class="fa fa-user-plus"></i>
                </a>
            </div>
            @end
        </div>
        <div class="box-body">
            <table id="membersTable" class="table table-striped">
                <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Current Address</th>
                    <th>Contact No</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>
                            <a href="{{ route('dashboard.members.show', $member->id) }}"> {{ $member->getFullName() }}</a>
                        </td>
                        @if($member->curr_country == '0')
                            <td>{{ $member->curr_vdc.' '.$member->curr_ward.', '.$member->curr_district }}</td>
                        @else
                            <td>{{ $member->city.', '.\App\Http\Utilities\Country::getCountry($member->curr_country) }}</td>
                        @endif
                        <td>{{ $member->contact_no }}</td>
                        @if ($member->membership->showMembershipStatus() == 'Expired')
                            <td><span class="bg-red">{{ $member->membership->showMembershipStatus() }}</span>
                                <a href="{{ route('dashboard.member.payment.create', $member->id) }}"
                                   class="btn btn-xs btn-primary">Renew</a>
                            </td>
                        @else
                            <td> {{ $member->membership->showMembershipStatus() }}</td>
                        @endif
                        <td>
                            <div class="pull-center">{!! $member->action_buttons !!}</div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#membersTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection