<div class="row">

    <div class="col-md-12">
        <!-- Registration Date Form Text Input -->
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('registration_date') ? ' has-error' : '' }}">
                {!! Form::label('registration_date','Registration Date ',['class' =>'col-md-4 control-label', 'required']) !!}

                <div class="col-md-6">
                    {!! Form::text('registration_date',date('Y-m-d'),['class'=>'form-control']) !!}

                    @if ($errors->has('registration_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('registration_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        {{--<div class="form-group{{ $errors->has('renewal_date') ? ' has-error' : '' }}">--}}
            {{--{!! Form::label('renewal_date','Renewal Date ',['class' =>'col-md-2 control-label', 'required']) !!}--}}

            {{--<div class="col-md-3">--}}
                {{--{!! Form::text('renewal_date',date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 365 day")),['class'=>'form-control']) !!}--}}

                {{--@if ($errors->has('renewal_date'))--}}
                    {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('renewal_date') }}</strong>--}}
                    {{--</span>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--Membership Type Form Radio Input --}}
        <div class="form-group{{ $errors->has('membership_type') ? ' has-error' : '' }}">
            {!! Form::label('membership_type','Membership Type ',['class' =>'col-md-2 control-label', 'required']) !!}

            <div class="col-md-3">
                <select class="form-control" id="membership_type" name="membership_type" required>
                    <option value="">Select Membership Type</option>
                    <option value="G">General</option>
                    <option value="A">Active</option>
                </select>

                @if ($errors->has('membership_type'))
                    <span class="help-block">
                                    <strong>{{ $errors->first('membership_type') }}</strong>
                                </span>
                @endif
            </div>
        </div>

        <!-- Verified Document Form Text Input -->
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('verified_document') ? ' has-error' : '' }}">
                {!! Form::label('verified_document','Verified Document ',['class' =>'col-md-4 control-label', 'required']) !!}

                <div class="col-md-6">
                    <select class="form-control" id="verified_document" name="verified_document" required>
                        <option value="">Select Document Type</option>
                        <option value="citizenship">Citizenship</option>
                        <option value="passport">Passport</option>
                    </select>

                    @if ($errors->has('verified_document'))
                        <span class="help-block">
                            <strong>{{ $errors->first('verified_document') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <!-- Document Number Form Text Input -->
        <div class="form-group{{ $errors->has('document_number') ? ' has-error' : '' }}">
            {!! Form::label('document_number','Document Number ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-3">
                {!! Form::input('number','document_number',null,['class'=>'form-control'], 'required') !!}

                @if ($errors->has('document_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('document_number') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <!-- Upload Document Form Text Input -->
        {{--<div class="form-group{{ $errors->has('document_image') ? ' has-error' : '' }}">--}}
        {{--{!! Form::label('document_image','Upload Document ',['class' =>'col-md-2 control-label']) !!}--}}

        {{--<div class="col-md-4">--}}
        {{--{!! Form::input('file','document_image',null,['class'=>'form-control', 'required']) !!}--}}

        {{--@if ($errors->has('document_image'))--}}
        {{--<span class="help-block">--}}
        {{--<strong>{{ $errors->first('document_image') }}</strong>--}}
        {{--</span>--}}
        {{--@endif--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
