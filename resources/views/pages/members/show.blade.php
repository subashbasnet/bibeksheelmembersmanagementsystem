@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.header') }}
        <small>{{ trans('labels.admin.members.show') }}</small>
    </h1>
@endsection

@section('content')
    <div id="page-content" class="profile2">
        <div class="bg-success clearfix">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-2">
                        <img class="profile-image" src="{{ asset('/assets/img/avatar.jpg') }}"/>
                        {{--<img class="profile-image"--}}
                        {{--src="{{ Gravatar::fallback(asset('/img/avatar5.png'))->get(Auth::user()->email, ['size'=>400]) }}"--}}
                        {{--alt="">--}}
                    </div>
                    <div class="col-md-9">
                        <h4 class="name">{{ $member->getFullName() }}</h4>
                        <div class="row stats">
                            <div class="col-md-6 stat">
                                <div class="label2" data-toggle="tooltip" data-placement="top"
                                     title="Association">{{ $member->getAssociation() }}</div>
                            </div>
                            <div class="col-md-6 stat">Current Address:<br/>
                                <i class="fa fa-map-marker"></i> {{ $member->curr_vdc.' '.$member->curr_ward.', '.$member->curr_district }}
                            </div>
                        </div>
                        <h4 class="dats1"><i class="fa fa-empire"></i> {{ $member->profession }}</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="dats1"><i class="fa fa-envelope-o"></i> {{ $member->email }}</h4>
                <h4 class="dats1"><i class="fa fa-phone"></i> {{ $member->mobile_no }}</h4>
                <div class="dats1"><i class="fa fa-clock-o"></i> Member Since:
                    <div class="label2" data-toggle="tooltip" data-placement="top" title="{{ $member->membership->registration_date }}">
                        <b>{{ toNepaliDate($member->membership->registration_date,'yyyy, Mon dd') }}</b>
                    </div>
                </div>
            </div>
        </div>
        <!-- Default box -->
        <ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
            <li class="active"><a role="tab" class="name" href="#tab_1" data-toggle="tab" aria-expanded="true">Membership
                    Detail</a></li>
            <li class=""><a role="tab" class="name" href="#tab_2" data-toggle="tab" aria-expanded="true">Personal
                    Detail</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                @include('pages.members.partials._member_membership_details')
            </div>
            <div class="tab-pane" id="tab_2">
                @include('pages.members.partials._member_personal_details')
            </div>
        </div>
    </div>
@endsection
