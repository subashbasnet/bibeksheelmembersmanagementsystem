<div class="row">
    <div class="col-md-12">

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title','Title ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                {!! Form::input('title','title',$event->title,['class'=>'form-control', 'required']) !!}

                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('desc','Event Description ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                {!! Form::textarea('desc',$event->desc,['class'=>'form-control', 'rows' => '3']) !!}

                @if ($errors->has('desc'))
                    <span class="help-block">
                        <strong>{{ $errors->first('desc') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
            {!! Form::label('start','Start Date ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                {!! Form::input('start','start',$event->start,['class'=>'form-control', 'required']) !!}

                @if ($errors->has('start'))
                    <span class="help-block">
                        <strong>{{ $errors->first('start') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="md-col-6">
            <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
                {!! Form::label('end','End Date ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-4">
                    {!! Form::input('end','end',$event->end,['class'=>'form-control', 'required']) !!}

                    @if ($errors->has('end'))
                        <span class="help-block">
                        <strong>{{ $errors->first('end') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group{{ $errors->has('background_color') ? ' has-error' : '' }}">
            {!! Form::label('background_color','Background Color',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                {!! Form::input('background_color','background_color',$event->background_color,['class'=>'form-control']) !!}

                @if ($errors->has('background_color'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('event_type') ? ' has-error' : '' }}">
            {!! Form::label('event_type','Event Type ',['class' =>'col-md-2 control-label', 'required']) !!}

            <div class="col-md-3">
                <select class="form-control select2" id="event_type" data-placeholder="Select Event Type" name="event_type" required>
                    <option value="event">Event</option>
                    <option value="meeting">Meeting</option>
                    <option value="campaign">Campaign</option>
                    <option value="training">Training</option>
                    <option value="other">Other</option>
                </select>

                @if ($errors->has('event_type'))
                    <span class="help-block">
                        <strong>{{ $errors->first('event_type') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('event_coordinator') ? ' has-error' : '' }}">
            {!! Form::label('event_coordinator','Event Coordinator',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-4">
                <select name="event_coordinator[]" multiple="multiple" id="event_coordinator" data-placeholder="Select Campaign Co-ordinator"  class="form-control select2">
                    @foreach($peoples as $people)
                        <option>{{ $people->getFullName() }}</option>
                    @endforeach
                </select>

                @if ($errors->has('event_coordinator'))
                    <span class="help-block">
                        <strong>{{ $errors->first('event_coordinator') }}</strong>
                    </span>
                @endif
            </div>

        </div>

        <fieldset id="campaign_five_Star">
            <legend>Campaign Five Star</legend>


            <div class="form-group{{ $errors->has('fund_raiser') ? ' has-error' : '' }}">
                {!! Form::label('fund_raiser','Fund Raising Manager',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-4">
                    <select name="fund_raiser" multiple="multiple" id="fund_raiser" data-placeholder="Select Fund Raising Manager"  class="form-control select2">
                        @foreach($peoples as $people)
                            <option value="{{ $people->id }}">{{ $people->getFullName() }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('fund_raiser'))
                        <span class="help-block">
                        <strong>{{ $errors->first('fund_raiser') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('logistic_manager') ? ' has-error' : '' }}">
                {!! Form::label('logistic_manager','Logistic Manager',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-4">
                    <select name="logistic_manager" multiple="multiple" id="media_manager" data-placeholder="Select Logistic Manager"  class="form-control select2">
                        @foreach($peoples as $people)
                            <option value="{{ $people->id }}">{{ $people->getFullName() }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('logistic_manager'))
                        <span class="help-block">
                        <strong>{{ $errors->first('logistic_manager') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('media_manager') ? ' has-error' : '' }}">
                {!! Form::label('media_manager','Media Manager',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-4">
                    <select name="media_manager" multiple="multiple" id="media_manager" data-placeholder="Select Media Manager"  class="form-control select2">
                        @foreach($peoples as $people)
                            <option value="{{ $people->id }}">{{ $people->getFullName() }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('media_manager'))
                        <span class="help-block">
                        <strong>{{ $errors->first('media_manager') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('volunteer_manager') ? ' has-error' : '' }}">
                {!! Form::label('volunteer_manager','Volunteer Manager',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-4">
                    <select name="volunteer_manager" multiple="multiple" id="media_manager" data-placeholder="Select Volunteer Manager"  class="form-control select2">
                        @foreach($peoples as $people)
                            <option value="{{ $people->id }}">{{ $people->getFullName() }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('volunteer_manager'))
                        <span class="help-block">
                        <strong>{{ $errors->first('volunteer_manager') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        </fieldset>

        <div class="form-group">
            <div class="pull-right col-xs-10 ">
                <button type="reset" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Reset
                </button>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> {{ $submitText }}
                </button>
            </div>
        </div>
    </div>
</div>