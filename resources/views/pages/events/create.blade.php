@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.events.header') }}
        <small>{{ trans('labels.admin.events.create') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/datepicker.css') }}">
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::open(['route' => 'dashboard.events.store', 'class' => 'form-horizontal', 'id' => 'form-wizard' , 'files'=>'true']) !!}
            @include('pages.events.partials._events_AddForm')
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')
    {!! Html::script('assets/js/moment.min.js') !!}
    {!! Html::script('assets/js/datepicker.js') !!}
    {!! Html::script('assets/js/event.js') !!}
    {!! Html::script('assets/js/select2.full.min.js') !!}
    <script type="text/javascript">
        $('#start').datepicker({
            timepicker: true
        });
        $('#end').datepicker({
            timepicker: true
        });
        $(".select2").select2();
        function toggleFields() {
            if ($("#event_type").val() == 'campaign')
            {
                $("#fund_raiser").prop('required',true);
                $("#campaign_five_Star").show();
            }
            else
            {
                $("#campaign_five_Star").hide();
                $("#fund_raiser").prop('required',false);
            }
        }
        $(document).ready(function () {
            toggleFields();

            $("#event_type").change(function () {
                toggleFields();
            });
        });
    </script>
@endsection