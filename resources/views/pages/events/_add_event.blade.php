<div class="row">
    <div class="col-md-9">

        <!-- Event Name Form Text Input -->
        <div class="form-group{{ $errors->has('event_name') ? ' has-error' : '' }}">
            {!! Form::label('event_name','Event Name ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-8">
                {!! Form::text('event_name',null,['class'=>'form-control']) !!}

                @if ($errors->has('event_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('event_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <!-- Description Form Text Input -->
        <div class="form-group{{ $errors->has('event_desc') ? ' has-error' : '' }}">
            {!! Form::label('event_desc','Description ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-8">
                {!! Form::textarea('event_desc',null,['size' => '10x5', 'class'=>'form-control']) !!}

                @if ($errors->has('event_desc'))
                    <span class="help-block">
                        <strong>{{ $errors->first('event_desc') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date_time_range') ? ' has-error' : '' }}">
            {!! Form::label('event_date_time','Event Date & Time ',['class' =>'col-md-2 control-label']) !!}

            <div class="col-md-8">
                <input type="text" class="form-control pull-right" id="event_date_time">

                @if ($errors->has('event_date_time'))
                    <span class="help-block">
                        <strong>{{ $errors->first('event_date_time') }}</strong>
                    </span>
                @endif
            </div>
            <!-- /.input group -->
        </div>

    </div>
</div>

<div class="form-group">
    <div class="pull-left col-xs-3 ">
        <button type="reset" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> Reset
        </button>
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> {{ $submitText }}
        </button>
    </div>
</div>
