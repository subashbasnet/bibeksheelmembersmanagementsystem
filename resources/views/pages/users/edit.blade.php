@extends('layouts.template')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="left pannel-heading">
                    <div class="page-title">
                        <h4>
                            <img class="icon-bar" src="{{ url('/assets/img/edit_user.png') }}" height="25" width="25"></image>
                            Edit Member
                        </h4>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                {!! Form::model($user, ['method' => 'PUT','route' => ['users.update',$user->id], 'class' => 'form-horizontal']) !!}
                    @include('pages.users._edit_user')
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="left pannel-heading">
                        <div class="page-title">
                            <h4>
                                Change Password
                            </h4>
                        </div>
                    </div>
                </div>


                <div class="panel-body">
                    {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal']) !!}
                    @include('pages.users._change_password')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <div class="left pannel-heading">
                        <div class="page-title">
                            <h4>
                                Modify Role
                            </h4>
                        </div>
                    </div>
                </div>


                <div class="panel-body">
                    {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal']) !!}
                    @include('pages.users._change_password')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @include('errors.validError')
    </div>
@endsection