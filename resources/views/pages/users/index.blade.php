@extends('layouts.template')

@section('content')
    <div class="tab-content">
        <div id="users" class="tab-pane fade in active">
            <div class="panel panel-default">
                <div class="panel-heading" style="height: 70px">
                    <div class="left pannel-heading" style="float: left;">
                        <div class="page-title">
                            <h4>MMS Users</h4>
                        </div>
                    </div>
                    <div class="right_pannel-heading" style="float: right">
                        {!! Form::open(['method' => 'GET', 'action' => ['UsersController@index'], 'class'=> "navbar-form navbar-left"]) !!}
                        <div class="form-group">
                            <span class="icon"><i class="fa fa-search"></i></span>
                            {!! Form::input('search','search',null,['id'=> 'searchBox', 'class'=>'form-control', 'placeholder'=>'Search...']) !!}
                        </div>
                        <input type="submit" hidden/>
                        {!! Form::close() !!}
                        <a href="{{ action('UsersController@create')  }}" class="btn btn-primary">Add</a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="membes-pannel">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Full Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td><input type="checkbox"/></td>
                                            <td><a href="{{ url('users',$user->id) }}"> {{ $user->full_name }}</a></td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ ucfirst(implode(', ',$user->getRoles())) }}</td>
                                            <td>{{ $user->status }}</td>
                                            <td>
                                                <div class="actions col-md-9">
                                                    <div class="col-md-4 col-offset-1">
                                                        <a href="{!! url('/users/'.$user->id.'/edit')  !!}" class="btn btn-success">Edit</a>
                                                    </div>
                                                    <div class="col-md-4">
                                                        {!! action_for('Delete', $user, 'DELETE') !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection