<div class="row">
    <div class="col-md-6">

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name','Name of Person',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('text','name',$alliance->name,['class'=>'form-control']) !!}

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">
            {!! Form::label('post','Post ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('text','post',$alliance->post,['class'=>'form-control']) !!}

                @if ($errors->has('post'))
                    <span class="help-block">
                        <strong>{{ $errors->first('post') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('organization') ? ' has-error' : '' }}">
            {!! Form::label('organization','Organization Name ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('text','organization',$alliance->organization,['class'=>'form-control']) !!}

                @if ($errors->has('organization'))
                    <span class="help-block">
                        <strong>{{ $errors->first('organization') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            {!! Form::label('address','Address ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::textarea('address',$alliance->address,['class'=>'form-control', 'rows' => '2']) !!}

                @if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email','Email ID (Personal) ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('email','email',$alliance->email,['class'=>'form-control']) !!}

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
            {!! Form::label('mobile','Mobile No.',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('phone','mobile',$alliance->mobile,['class'=>'form-control']) !!}

                @if ($errors->has('mobile'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('phone_1') ? ' has-error' : '' }}">
            {!! Form::label('phone_1','Primary Telephone',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('phone_1','phone_1',$alliance->mobile,['class'=>'form-control']) !!}

                @if ($errors->has('phone_1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone_1') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('phone_2') ? ' has-error' : '' }}">
            {!! Form::label('phone_2','Secondary Telephone',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('phone_2','phone_2',$alliance->mobile,['class'=>'form-control']) !!}

                @if ($errors->has('phone_2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone_2') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('alliance_type') ? ' has-error' : '' }}">
            {!! Form::label('alliance_type','Alliance Type ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-6">
                <select name="payment_modes" id="payment_modes" class="form-control">
                    <option value="regular">Regular</option>
                    <option value="regular">Anti Corruption Alliance</option>
                </select>

                @if ($errors->has('alliance_type'))
                    <span class="help-block">
                    <strong>{{ $errors->first('alliance_type') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
            {!! Form::label('website','Website ',['class' =>'col-md-4 control-label']) !!}

            <div class="col-md-8">
                {!! Form::input('text','website',$alliance->website,['class'=>'form-control']) !!}

                @if ($errors->has('website'))
                    <span class="help-block">
                        <strong>{{ $errors->first('website') }}</strong>
                    </span>
                @endif
            </div>
        </div>

    </div>
</div>


<div class="form-group">
    <div class="pull-right col-xs-10 ">
        <button type="reset" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> Reset
        </button>
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> {{ $submitText }}
        </button>
    </div>
</div>
