@extends('dashboard.layouts.master')

@section ('title', trans('labels.admin.alliances.header') . ' | ' . trans('labels.admin.alliances.create'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.alliances.header') }}
        <small>{{ trans('labels.admin.alliances.create') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::open(['route' => 'dashboard.alliances.store', 'class' => 'form-horizontal', 'id' => 'form-wizard' , 'files'=>'true']) !!}
                @include('pages.alliances.partials._alliances_AddForm')
            {!! Form::close() !!}
        </div>
        <div class="box-footer"></div>
    </div>
@endsection

@section('after-scripts-end')

@endsection