@extends('dashboard.layouts.master')

@section ('title', trans('labels.admin.alliances.header') . ' | ' . trans('labels.admin.alliances.create'))

@section('page-header')
    <h1>
        {{ trans('labels.admin.alliances.header') }}
        <small>{{ trans('labels.admin.alliances.edit') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::model($alliance, ['method' => 'PUT','route' => ['dashboard.alliances.update',$alliance->id], 'class' => 'form-horizontal']) !!}
                @include('pages.alliances.partials._alliances_EditForm')
            {!! Form::close() !!}
        </div>
        <div class="box-footer"></div>
    </div>
@endsection

@section('after-scripts-end')

@endsection