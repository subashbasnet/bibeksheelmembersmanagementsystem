@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.alliances.header') }}
        <small>{{ trans('labels.admin.alliances.show') }}</small>
    </h1>
@endsection

@section('content')
    <div id="page-content" class="profile2">
        <div class="bg-success clearfix">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <img class="profile-user-img" src="{{ asset('/assets/img/avatar.jpg') }}"/>
                        {{--<img class="profile-image"--}}
                        {{--src="{{ Gravatar::fallback(asset('/img/avatar5.png'))->get(Auth::user()->email, ['size'=>400]) }}"--}}
                        {{--alt="">--}}
                    </div>
                    <div class="col-md-8">
                        <h4 class="name">{{ $alliance->name }}</h4>
                        <div class="row stats">
                            <div class="col-md-6 stat">
                                <p class="desc">{{ $alliance->post }}
                                <div class="label2" data-toggle="tooltip" data-placement="top"
                                     title="Organization">{{ $alliance->organization }}</div>
                            </div>
                            <div class="col-md-6 stat">Address:<br/>
                                <i class="fa fa-map-marker"></i> {{ $alliance->address }}
                            </div>
                        </div>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h4 class="dats1"><i class="fa fa-envelope-o"></i> {{ $alliance->email }}</h4>
                <h4 class="dats1"><i class="fa fa-mobile-phone"></i> {{ $alliance->mobile }}</h4>
                <div class="dats1"><i class="fa fa-phone"></i> {{ $alliance->phone_1 }}</div>
            </div>
            <div class="col-md-3">
                <div class="dats1"><i class="fa fa-globe"></i> {{ $alliance->website }}</div>
            </div>
        </div>
        <ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
            <li class=""><a href="{{ route('dashboard.alliances.index') }}" data-toggle="tooltip" data-placement="right" title="Back to Members"><i class="fa fa-chevron-left"></i></a></li>
            <li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
            <li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Timeline</a></li>
        </ul>
    </div>
@endsection
