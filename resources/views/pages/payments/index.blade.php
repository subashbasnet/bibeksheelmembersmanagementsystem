@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.payment.header') }}
        <small>{{ trans('labels.admin.members.payment.view') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    @permission('create-payments')
                        <a class="btn btn-xs btn-primary" href="{{ route('dashboard.member.payment.create', $member->id) }}">
                            <i class="fa fa-plus"></i>
                        </a>
                    @end
                    <a class="btn btn-xs btn-green" href="window.print()">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table id="paymentsDetail" class="table">
                    <tbody>
                    <tr>
                        <td><label>Members Name: </label></td>
                        <td>{{ $member->getFullName() }}</td>
                        @if($member->getRegistrationDate())
                            <td><label>Member Since: </label></td>
                            <td>{{ $member->getRegistrationDate() }}</td>
                        @else
                            <td>Association Type</td>
                            <td>{{ $member->getAssociation() }}</td>
                        @endif
                        @if($member->getMembershipExpiryDate())
                            <td><label>Membership Expire Date: </label></td>
                            <td>{{ $member->getMembershipExpiryDate() }}</td>
                        @endif
                    </tr>
                    </tbody>
                </table>
                <hr />
                <table id="paymentsTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <td>Payment Date</td>
                        <td>Payment Purpose</td>
                        <td>Modes of Payment</td>
                        <td>Payment Amt (Rs.)</td>
                        <td>Receipt No.</td>
                        <td>Received By</td>
                        <td>Edit</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $payment)
                        <tr>
                            <td>{{ $payment->payment_date }}</td>
                            <td>{{ $payment->purpose }}</td>
                            <td>{{ $payment->payment_modes }}</td>
                            <td>{{ $payment->payment_amount }}</td>
                            <td>{{ $payment->receipt_id }}</td>
                            <td>{{ $payment->received_by }}</td>
                            <td>
                                @permission('edit-payments')
                                    <a href="{{ route('dashboard.member.payment.edit', [$member->id, $payment->id]) }}" class="btn btn-xs btn-primary">
                                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.general.crud.edit') }}"></i>
                                    </a>
                                @end
                                @permission('delete-payments')
                                    <a href="{{ route('dashboard.member.payment.destroy', [$member->id, $payment->id]) }}" class="btn btn-xs btn-danger" data-method="delete">
                                        <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ trans('buttons.general.crud.delete') }}"></i>
                                    </a>
                                @end
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
            </div>
        </div>
@endsection

@section('after-scripts-end')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#paymentsTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection