@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.members.payment.header') }}
        <small>{{ trans('labels.admin.members.payment.create') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.admin.members.payment.add') }}</h3>
        </div>
        <div class="box-body">
            {!! Form::open(['method' => 'POST','route' => ['dashboard.member.payment.store', $member->id], 'class' => 'form-horizontal']) !!}
                @include('pages.payments._add_payment')
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('after-scripts-end')
    <script>
        $(document).ready(function () {
            toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
            //this will call our toggleFields function every time the selection value of our underAge field changes
            $("#purpose").change(function () {
                toggleFields();
            });

            $("#payment_modes").change(function () {
                toggleFields();
            });
        });
        function toggleFields() {
            if ($("#purpose").val() == 'Membership Fee') {
                $("#membership_types").show();
                $("#membership_year").show();
            }
            else {
                $("#membership_types").hide();
                $("#membership_year").hide();
            }
        }

    </script>
@endsection