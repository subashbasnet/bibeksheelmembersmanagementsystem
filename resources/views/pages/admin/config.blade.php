@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.setting.header') }}
        <small>{{ trans('labels.admin.setting.config') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title"></div>

            <div class="box-body">
            {!! Form::open(['route' => 'admin.setting.store', 'class' => 'form-group', 'files'=>'true']) !!}
            <!-- text input -->
                <div class="form-group">
                    <label>Website Name</label>
                    <input type="text" class="form-control" placeholder="Website Name" name="sitename"
                           value="{{ $configs['sitename'] }}">
                </div>
                <div class="form-group">
                    <label>Website Short Name</label>
                    <input type="text" class="form-control" placeholder="Short Sitename" maxlength="2"
                           name="sitename_short" value="{{$configs['sitename_short']}}">
                </div>
                <div class="form-group">
                    <label>Site Description</label>
                    <input type="text" class="form-control" placeholder="Description in 140 Characters"
                           maxlength="140" name="site_description" value="{{$configs['site_description']}}">
                </div>
                <div class="form-group">
                    <label>Logo</label>
                    <input type="file" class="file file-upload" name="logo">
                    <img src="{{ asset('storage/app/public/logo.png') }}" alt="logo" style="width:200px; height:auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                </div>
                <!-- checkbox -->
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="sidebar_search"
                                   @if($configs['sidebar_search']) checked @endif>
                            Show Search Bar
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="show_messages"
                                   @if($configs['show_messages']) checked @endif>
                            Show Messages Icon
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="show_notifications"
                                   @if($configs['show_notifications']) checked @endif>
                            Show Notifications Icon
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="show_tasks"
                                   @if($configs['show_tasks']) checked @endif>
                            Show Tasks Icon
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="show_rightsidebar"
                                   @if($configs['show_rightsidebar']) checked @endif>
                            Show Right SideBar Icon
                        </label>
                    </div>
                </div>
                <!-- select -->
                <div class="form-group">
                    <label>Skin Color</label>
                    <select class="form-control" name="skin">
                        @foreach($skins as $name=>$property)
                            <option value="{{ $property }}"
                                    @if($configs['skin'] == $property) selected @endif>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Layout</label>
                    <select class="form-control" name="layout">
                        @foreach($layouts as $name=>$property)
                            <option value="{{ $property }}"
                                    @if($configs['layout'] == $property) selected @endif>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Admin Email</label>
                    <input type="text" class="form-control" placeholder="To send emails to others via SMTP"
                           maxlength="100" name="default_email" value="{{$configs['admin_email']}}">
                </div>

                <div class="form-group">
                    <label>Website</label>
                    <input type="text" class="form-control" placeholder="To send emails to others via SMTP"
                           maxlength="100" name="default_email" value="{{$configs['website']}}">
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
    </div>
@endsection


@section('after-scripts-end')

@endsection