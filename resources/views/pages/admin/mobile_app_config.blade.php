@extends('dashboard.layouts.master')

@section('page-header')
    <h1>
        {{ trans('labels.admin.setting.header') }}
        <small>{{ trans('labels.admin.setting.mobile_config') }}</small>
    </h1>
@endsection

@section('after-styles-end')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.css') }}">
@endsection

@section('content')
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title"></div>

            <div class="box-body">
            {!! Form::open(['route' => 'admin.setting.store', 'class' => 'form-group', 'files'=>'true']) !!}
            <!-- text input -->
                <div class="form-group">
                    <label>App Name</label>
                    <input type="text" class="form-control" placeholder="App Name" name="mobile_app_name"
                           value="{{ $configs['mobile_app_name'] or '' }}">
                </div>
                <div class="form-group">
                    <label>App Short Name</label>
                    <input type="text" class="form-control" placeholder="Short App Name" maxlength="2"
                           name="app_name_short" value="{{$configs['app_name_short'] or ''}}">
                </div>
                <div class="form-group">
                    <label>App Description</label>
                    <textarea class="form-control" placeholder="Description in 140 Characters"
                              maxlength="140" name="app_description"
                              value="{{$configs['app_description'] or ''}}"></textarea>
                </div>
                <div class="form-group">
                    <label>About Bibeksheel Nepali</label>
                    <textarea class="form-control" placeholder="About Bibeksheel Nepali" name="about_bibeksheel"
                              value="{{$configs['about_bibeksheel'] or ''}}"></textarea>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection


@section('after-scripts-end')

@endsection